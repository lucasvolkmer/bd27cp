# ATIVIDADE PRÁTICA SUPERVISIONADA DE BANCO DE DADOS 2
### UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARNÁ
### ENGENHARIA DE COMPUTAÇÃO

### Professor: Ives Pola
### Alunos: 
-Fábio Henrique Kurpel
-Lucas Volkmer Hendges

## 1) INTRODUÇÃO
O seguinte projeto foi desenvolvido para atender aos requisitos parciais de avaliação da disciplina de Banco de Dados 2. 

Foi desenvolvido um banco de dados relacional cujo objetivo é armazenar uma série de dados sobre os personagens, casas, feitiços e varinhas do Universo fictício de Harry Potter.

O projeto foi desenvolvido utilizando o banco de dados PostgreSQL, programado com a ferramenta PgAdmin.

## 2) Diagrama Entidade-Relacionamento e Modelagem Relacional
Foi desenvolvido o Diagrama Entidade-Relacionamento (DER) baseado nas ideias iniciais para o banco de dados. O resultado é conforme a Figura 1.
 ![](imagens/DER.jpg)

*Figura 1: Diagrama Entidade-Relacionamento para o banco de dados.*

Pelo DER do banco de dados, obtém-se o modelo relacional do mesmo, que expressa as relações pertinentes em forma de tabelas, assim como estabelece as relações de dependência entre elas (as chaves estrangeiras).

A partir da imagem, pode-se perceber que existem alguns atributos relacionais que poderiam ser aplicados no banco de dados, como a data de criação de uma **Casa**. Porém, dada a natureza da relação entre as tabelas **Casa** e **Personagem**, por exemplo, esta relação não se torna tabela na modelagem, por não ser uma relação N para M.

Por consequência, a única relação que se torna tabela é a **Saber**, visto que um personagem pode saber mais de um feitiço, assim como um feitiço pode ser conhecido por mais de um personagem.

Mais detalhes sobre o modelo relacional podem ser observados na Figura 2.
 ![](imagens/Modelo_Relacional.jpg)

*Figura 2: Modelo Relacional para o banco de dados.*

Nas tabelas abaixo constam a relação de todos os atributos, e o que cada um deles representa:

**FEITICO**

| Atributo     | Descrição                       |
|--------------|---------------------------------|
| **feiticoPK**| Chave primária da tabela        |
| nomeFeitico  | Nome do feitiço                 |
| descricao    | Descrição do que o feitiço faz  |
| alcance      | Alcance fictício do feitiço     |

**SABER**

| Atributo        | Descrição                                                                 |
|-----------------|---------------------------------------------------------------------------|
| **feiticoFK**   | Chave estrangeira que referencia feitiço e também parte da chave primária |
| **personagemFK**| Chave estrangeira que personagem feitiço e também parte da chave primária |
| dataAprendeu    | Data em que aprendeu o feitiço                                            |

**PERSONAGEM**

| Atributo        | Descrição                                                                 |
|-----------------|----------------------------------------------------------|
|**PersonagemPK** | Chave primária da tabela                                 |
|Nome             | Nome do personagem                                       |
|DataNasc         | Data de nascimento do personagem                         |
|PontosAcumulados | Pontos acumulados que serão utilizados na taça das casas |
|CasaFK           | A casa na qual o aluno foi selecionado                   |

**VARINHA**


| Atributo       | Descrição                                          |
|----------------|----------------------------------------------------|
|**VarinhaPK**   | Chave primária da tabela                           |
|Material        | Material que constitui a varinha                   |
|Nucleo          | Material utilizado como núcleo mágico da varinha   |
|Medida          | Medida da varinha em polegadas                     |
|Flex            | Flexibilidade da varinha                           |
|PersonagemDonoPK| Chave estrangeira que referencia o dono da varinha |

**CASA**


| Atributo   | Descrição                                                    |
|------------|--------------------------------------------------------------|
|**CasaPK**  | Chave primária da tabela                                     |
|Pontos      | Pontos totais da casa que serão utilizados na taça das casas |
|Nome        | Nome da casa                                                 |
|Descricao   | Descrição dos integrantes da casa                            |
|FundadorFK  | Fundador da casa                                             |

Observando as tabelas **Casa** e **Personagem**, é possível perceber que elas possuem uma dependência cruzada no que se refere à chave estrangeira na tabela **Personagem**. Para isso é necessário um tratamento especial, que será abordado na seção 3.

## 3) Criação das tabelas

Para a criação das tabelas, o seguinte código em SQL foi executado:

~~~
DROP TABLE IF EXISTS Saber CASCADE;
DROP TABLE IF EXISTS Personagem CASCADE;
DROP TABLE IF EXISTS Varinha CASCADE;
DROP TABLE IF EXISTS Casa CASCADE;
DROP TABLE IF EXISTS Feitico CASCADE;

CREATE TABLE IF NOT EXISTS Personagem(
	personagemPK integer PRIMARY KEY,
	nomePersonagem varchar(50) NOT NULL,
	dataNasc date,
	pontosAcumulados integer,
	casaFK integer
);
CREATE TABLE IF NOT EXISTS Casa(
	casaPK integer PRIMARY KEY,
	nomeCasa varchar(15) NOT NULL,
	pontos integer NOT NULL,
	descricao varchar(200), 
	fundadorFK integer REFERENCES Personagem(personagemPK)
);
ALTER TABLE Personagem ADD FOREIGN KEY (casaFK) REFERENCES Casa(casaPK);

CREATE TABLE IF NOT EXISTS Varinha(
	varinhaPK integer PRIMARY KEY,
	material varchar(20) NOT NULL,
	nucleo varchar(20) NOT NULL,
	medida float,
	flex varchar(20),
	donoFK integer REFERENCES Personagem(personagemPK)
);
CREATE TABLE IF NOT EXISTS Feitico(
	feiticoPK integer PRIMARY KEY,
	nomeFeitico varchar(50) NOT NULL,
	descricao varchar(200),
	alcance integer
);
CREATE TABLE IF NOT EXISTS Saber(
	feiticoFK integer REFERENCES Feitico(feiticoPK),
	personagemFK integer REFERENCES Personagem(personagemPK),
	PRIMARY KEY (feiticoFK, personagemFK),
	dataAprendeu date
);
~~~

Pode-se perceber que a definição do atributo *casaFK* na tabela **Personagem** como chave estrangeira referenciando a tabela **Casa** é feito após a criação de ambas as tabelas, dada a relação cruzada comentada na seção anterior, que impossibilita a definição dessa chave estrangeira diretamente na criação da tabela.

Em seguida, as tabelas foram populadas com dados reais (provenientes de bancos de dados sobre o universo Harry Potter), acrescidos de alguns dados fictícios gerados randomicamente.

Abaixo há quatro trechos de código que inserem 3 tuplas em cada uma das tabelas tabelas **Casa**, **Personagem**, **Feitico**, **Varinha** e **Saber**, respectivamente.

~~~
INSERT INTO Casa VALUES (1, 'Corvinal', 0, 'A casa dos que tem a mente sempre alerta,
Onde os homens de grande espírito e saber
Sempre encontrarão companheiros seus iguais', null);
INSERT INTO Casa VALUES (2, 'Grifinória', 0, 'Casa onde habitam os corações indômitos;
Ousadia e sangue-frio e nobreza Destacam os alunos da Grifinória dos demais', null);
INSERT INTO Casa VALUES (3, 'Lufa-Lufa', 0, 'Onde seus moradores são justos e leais,
Pacientes, sinceros, sem medo da dor', null);
~~~

~~~
INSERT INTO Personagem VALUES (1, 'Harry James Potter', '31-7-1980', 0, 2);
INSERT INTO Personagem VALUES (73, 'Barty Crouch Jr.', '1-1-1962', 0, null);
INSERT INTO Personagem VALUES (117, 'Madame Olympe Maxime', null, 0, null);
~~~

~~~
INSERT INTO Feitico VALUES ( 3, 'Launch an object up into the air', 'Rockets target upward', 9);
INSERT INTO Feitico VALUES ( 20, 'Killing Curse', 'Instantaneous death', 7);
INSERT INTO Feitico VALUES ( 55, 'Blasting Curse', 'Explosion', 1);
~~~

~~~
INSERT INTO Varinha VALUES ( 9, 'Apple', 'Veela hair', 10.96, 'Very flexible', 70);
INSERT INTO Varinha VALUES ( 41, 'Hazel', 'Veela hair', 10.32, 'Little Flexivel', 41);
INSERT INTO Varinha VALUES ( 119, 'Cherry', 'Phoenix feather', 7.15, 'Very flexible', 33);
~~~

~~~
INSERT INTO Saber VALUES ( 216, 140, '28-9-1991');
INSERT INTO Saber VALUES ( 269, 115, '13-8-1990');
INSERT INTO Saber VALUES ( 165, 104, '4-6-2011');
~~~

Após a criação da tabela **Casa**, foram setadas as chaves estrangeiras referenciando os personagens fundadores de cada uma delas, conforme abaixo:
~~~
UPDATE Casa SET fundadorFK = 70 WHERE casaPK = 1;
UPDATE Casa SET fundadorFK = 69 WHERE casaPK = 2;
UPDATE Casa SET fundadorFK = 78 WHERE casaPK = 3;
UPDATE Casa SET fundadorFK = 68 WHERE casaPK = 4;
~~~

Outro ponto importante de ser ressaltado foi a necessidade de criação da primeira trigger para o banco de dados. Como o atributo *dataAprendeu* da tabela **Saber** (que relaciona os feitiços conhecidos por cada personagem) foi gerado através de uma data randômica, é possível que a data gerada não seja válida, pois poderia ser gerada uma data anterior à data de nascimento do personagem que sabe o feitiço.

Para isso, foi criada uma trigger que, antes de inserir uma tupla na tabela **Saber**, verifica se o valor de *dataAprendeu* é valido. Caso seja, apenas insere a tupla, caso não seja, atribui a data de nascimento do personagem acrescida de 11 anos para *dataAprendeu* (a escolha de 11 anos se deve ao contexto do Universo de Harry Potter, pois é a idade com a qual os bruxos ingressam na escola de magia).

O trecho de código que define essa trigger pode ser observado abaixo:

~~~
CREATE OR REPLACE FUNCTION dataValida ()
RETURNS TRIGGER AS $validar$
DECLARE
	nasc date;
	aprendeu date := NEW.dataAprendeu;
BEGIN
	SELECT dataNasc INTO nasc
	FROM Personagem
	WHERE personagemPK = NEW.personagemFK;

	IF extract(year from nasc) + 11 > extract(year from aprendeu) THEN
		NEW.dataAprendeu := nasc + interval '11 YEARS';
	END IF;
	RETURN NEW;
END;
$validar$ LANGUAGE plpgsql;
CREATE TRIGGER validar
BEFORE INSERT ON Saber
FOR EACH ROW EXECUTE PROCEDURE dataValida();
~~~

Todo o código de criação e população das tabelas, a que essa seção se refere, pode ser encontrado no arquivo CriarEPopularFinal.txt disponível nesse repositório.

## 4) Criação dos Indíces

Foram criados alguns índices para que as consultas no banco de dados tenham melhor desempenho. Para isso, foram observados os atributos de cada tabela que tivessem uma forte relação uns com os outros, ou que fossem mais prováveis de serem solicitados em uma consulta, e criou-se os índices sobre esses.

Antes da criação, foi executado o seguinte trecho de código para garantir o uso da indexação nas consultas:

~~~
SET enable_seqscan = OFF;
~~~

Em seguida foram criados os seguintes índices de consultas. Cada índice possui uma consulta que o utiliza, para fins de testes.

###### idxNomePers
Índice criado sobre o nome do personagem: 
~~~
DROP INDEX IF EXISTS idxNomePers;
CREATE INDEX IF NOT EXISTS idxNomePers ON Personagem (nomePersonagem);
EXPLAIN ANALYZE SELECT * FROM Personagem WHERE nomePersonagem = 'Harry James Potter';
~~~

##### idxPontosCasaPers
Índice criado sobre os pontos acumulados de um personagem e a chave estrangeira da sua casa:
~~~
DROP INDEX IF EXISTS idxPontosCasaPers;
CREATE INDEX IF NOT EXISTS idxPontosCasaPers ON Personagem (pontosAcumulados, casaFK);
EXPLAIN ANALYZE SELECT * FROM Personagem WHERE casaFK = 1;
~~~

##### idxVarinhaDono
Índice criado sobre a chave primária de uma varinha e a chave estrangeira para seu dono:
~~~
DROP INDEX IF EXISTS idxVarinhaDono;
CREATE INDEX IF NOT EXISTS idxVarinhaDono ON Varinha (varinhaPK, donoFK);
EXPLAIN ANALYZE SELECT * FROM Varinha WHERE varinhaPK = 1;
~~~

##### idxVarinhaCarac
Índice criado sobre as características físicas de uma varinha: material, núcleo, medida e flexibilidade:
~~~
DROP INDEX IF EXISTS idxVarinhaCarac;
CREATE INDEX IF NOT EXISTS idxVarinhaCarac ON Varinha (material, nucleo, medida, flex);
EXPLAIN ANALYZE SELECT * FROM Varinha WHERE material = 'Cherry';
~~~

##### idxFeiticoAlcance
Índice criado sobre a chave primária de um feitiço e seu alcance:
~~~
DROP INDEX IF EXISTS idxFeiticoAlcance;
CREATE INDEX IF NOT EXISTS idxFeiticoAlcance ON Feitico (feiticoPK, alcance);
EXPLAIN ANALYZE SELECT * FROM Feitico WHERE alcance > 8;
~~~

##### idxSaber
Índice criado sobre as chaves estrangeiras para personagem e feitiço, e a data que o personagem aprendeu o feitiço:
~~~
DROP INDEX IF EXISTS idxSaber;
CREATE INDEX IF NOT EXISTS idxSaber ON Saber (feiticoFK, personagemFK, dataAprendeu);
EXPLAIN ANALYZE SELECT * FROM Saber WHERE extract(year from dataAprendeu) > 1985;
~~~

Nesse repositório pode-se encontrar o arquivo Indexes.txt que define todos os índices acima.

## 5) Funções
Foram criadas três funções para realizar operações corriqueiras nas tabelas, com o intuito de agilizá-las.

A primeira das funções recebe a chave primária de um personagem e retorna uma tupla com os dados mais relevantes desse personagem. Para isso, foi feita uma junção entre as tabelas **Casa** e **Personagem** para obter dados da pontuação da casa.

A função e uma consulta que a utiliza podem ser observadas abaixo:

~~~
DROP FUNCTION IF EXISTS mostraDadosPersonagem;
CREATE OR REPLACE FUNCTION mostraDadosPersonagem(PK integer)
RETURNS TABLE (nome varchar(50), data_de_nascimento date, pontos_acumulados int,
			   casa varchar(15), pontos_da_casa int) AS $$
BEGIN
	RETURN QUERY SELECT Personagem.nomePersonagem,
		   Personagem.dataNasc,
		   Personagem.pontosAcumulados,
		   Casa.nomeCasa,
		   Casa.pontos
	FROM Personagem NATURAL JOIN Casa
	WHERE Personagem.personagemPK = PK AND
	   Personagem.casaFK = Casa.casaPK;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM mostraDadosPersonagem(50); -- Mostra dados do personagem cuja PK é 50
~~~

A segunda função é responsável por marcar pontos para um personagem e sua casa. Para isso, passa-se a chave primária do personagem e o número de pontos a ser computado. 

A lógica da função verifica se o atributo pontos se aplica ao personagem, e se sim, computa os pontos ao personagem e à sua casa.

O código que implementa essa função pode ser observado abaixo, bem como uma aplicação dela:

~~~
DROP FUNCTION IF EXISTS marcaPontos;
CREATE OR REPLACE FUNCTION marcaPontos(PK integer, pontos_inc integer)
RETURNS VOID AS $$
DECLARE
	pontos_personagem integer; -- número de pontos que o personagem tem atualmente
	personagem_casa integer;   -- PK da casa do personagem
	pontos_casa integer;       -- número de pontos que a casa tem atualmente
BEGIN
	SELECT pontosAcumulados INTO pontos_personagem
	FROM Personagem WHERE personagemPK = PK;
	SELECT casaFK INTO personagem_casa
	FROM Personagem WHERE personagemPK = PK;
	SELECT pontos INTO pontos_casa
	FROM Casa WHERE personagem_casa = casaPK;
-- Computa os valores novos para os pontos se a casa for válida
	IF personagem_casa IS NOT NULL THEN
		pontos_personagem = pontos_personagem + pontos_inc;
		pontos_casa = pontos_casa + pontos_inc;
	END IF;
-- Dá update nas tabelas
	UPDATE Personagem
	SET pontosAcumulados = pontos_personagem
	WHERE personagemPK = PK;
	UPDATE Casa
	SET pontos = pontos_casa
	WHERE casaPK = personagem_casa;
END;
$$ LANGUAGE plpgsql;
SELECT marcapontos(2, 5); 
~~~

A terceira função é responsável por zerar os pontos de uma casa, e por consequência zerar os pontos de todos os personagens que fazem parte dessa casa.

O código que a implementa e uma aplicação que a utiliza pode ser observado abaixo:

~~~
DROP FUNCTION IF EXISTS zerarPontos;
CREATE OR REPLACE FUNCTION zerarPontos(PK_zerar integer)
RETURNS VOID AS $$
BEGIN
	UPDATE Personagem
	SET pontosAcumulados = 0
	WHERE casaFK = PK_zerar;
	UPDATE Casa
	SET pontos = 0
	WHERE casaPK = PK_zerar;
END;
$$ LANGUAGE plpgsql;
--SELECT zerarPontos(2);
~~~

## 6) Triggers

Para que a integridade do banco de dados seja mantida foram criadas 3 Triggers, que garantem que os valores nas tabelas estão sempre atualizados e que coesos.

A primeira trigger se chama **TiggerPontosDasCasas**, ela será executada sempre que uma tupla for inserida na tabela Personagem. Essa trigger tem como objetivo recalcular os pontos totais da casa na qual o personagem inserido ou atualizado pertence. Ela é muito importante, pois sem esta Trigger os valores da tabela Casa podem não estar coerentes aos dados presentes na tabela Personagem.

~~~
CREATE OR REPLACE FUNCTION AtualizaPontos()
RETURNS trigger AS $teste$
DECLARE 
	var INT;
BEGIN
	SELECT SUM( PontosAcumulados ) INTO var FROM Personagem WHERE Personagem.CasaFK = NEW.CasaFK;
    UPDATE Casa SET Pontos = var WHERE CasaPK = NEW.CasaFK; 
    RETURN NULL;
END;
$teste$ LANGUAGE plpgsql;

--DROP TRIGGER TriggerPontosDasCasas ON Personagem CASCADE;
CREATE TRIGGER TriggerPontosDasCasas
AFTER INSERT OR UPDATE ON Personagem
FOR EACH ROW
EXECUTE PROCEDURE AtualizaPontos();
~~~

A segunda trigger criada foi **validar**. Devido aos dados de dataAprendeu na tabela Saber terem sido criados de maneira aleatória é possivel que o valor gerado para dataAprendeu seja antes da data de nascimento do personagem, nestes casos a trigger agirá de forma a alterar o atributo dataAprendeu para o aniverário de 11 anos do personagem, que é quando é permitido aos bruxos executar seus primeiros feitiços.


~~~
CREATE OR REPLACE FUNCTION dataValida ()
RETURNS TRIGGER AS $validar$
DECLARE
	nasc date;
	aprendeu date := NEW.dataAprendeu;
BEGIN
	SELECT dataNasc INTO nasc
	FROM Personagem
	WHERE personagemPK = NEW.personagemFK;

	IF extract(year from nasc) + 11 > extract(year from aprendeu) THEN
		NEW.dataAprendeu := nasc + interval '11 YEARS';
	END IF;
	RETURN NEW;
END;
$validar$ LANGUAGE plpgsql;

CREATE TRIGGER validar
BEFORE INSERT ON Saber
FOR EACH ROW EXECUTE PROCEDURE dataValida();
~~~

A terceira trigger criada será discutida na sessão abaixo, por se tratar de uma trigger que altera atributos auditoriais.

## 7) Visões e Auditoria

Foram criadas 3 visões com o objetivo de simplificar algumas consultas de dados e também limitar o acesso de dados de alguns usuários, tornando assim o banco de dados mais seguro. 

A primeira visão criada foi **houses**, nela é possivel acessar o nome do personagem, o nome da casa na qual o personagem pertence e sua pontuação individual.

~~~
CREATE MATERIALIZED VIEW houses (nome, casa, pontos) 
AS SELECT
	Personagem.NomePersonagem,
    Casa.NomeCasa,
    Personagem.PontosAcumulados
FROM Personagem JOIN casa
ON Personagem.casaFK = Casa.CasaPK;
~~~

Já na visão **wands** é possivel acessar informações importantes sobre personagem e as varinhas as quais possui. Essas informações são o nome do personagem e o material, núcleo e medidadas das varinhas que possui.

~~~
CREATE MATERIALIZED VIEW wands (nome, material, nucleo, medidas) 
AS SELECT
	Personagem.NomePersonagem,
    Varinha.Material,
    Varinha.Nucleo,
    Varinha.Medida
FROM Personagem JOIN Varinha
ON Personagem.personagemPK = Varinha.DonoFK;
~~~

A terceira visão criada foi a **spells**, através dela é possivel obter o nome do personagem juntamento com os feitiços que sabe e seus respectivos alcances.

~~~
CREATE MATERIALIZED VIEW spells (feitico, nome, alcance) 
AS SELECT
    Feitico.nomeFeitico,
	Personagem.nomePersonagem,
    Feitico.Alcance
FROM Feitico JOIN Saber ON Feitico.FeiticoPK = Saber.FeiticoFK
JOIN Personagem ON Personagem.PersonagemPK = Saber.PersonagemFK;
~~~

A tabela que optamos por auditar foi a tabela **Saber**, ela armazena os feitiços que cada personagem aprendeu e é muito importante analisa-la com proximidade, visto que alguns feitiços são proibidos.

A tabela de auditoria **Audit_saber** irá armazenar a operação realizada sobre a tabela, a data, o usuário que a realizou, o feitiço aprendido e o personagem responsável.

~~~
CREATE TABLE Audit_saber (operacao CHAR, data TIMESTAMP, usuario VARCHAR, feitico INTEGER, personagem INTEGER);

CREATE OR REPLACE FUNCTION process_saber_audit()
RETURNS TRIGGER AS $saber_audit$
BEGIN 
    IF (TG_OP = 'DELETE') THEN 
        INSERT INTO audit_saber SELECT 'D', NOW(), USER, OLD.feiticoFK, OLD.personagemFK;
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN 
        INSERT INTO audit_saber SELECT 'U', NOW(), USER, NEW.feiticoFK, NEW.personagemFK;
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN 
        INSERT INTO audit_saber SELECT 'I', NOW(), USER, NEW.feiticoFK, NEW.personagemFK;
        RETURN NEW;
    END IF;

    RETURN NULL;
END;
$saber_audit$ LANGUAGE plpgsql;

CREATE TRIGGER saber_audit
AFTER INSERT OR UPDATE OR DELETE ON saber
FOR EACH ROW EXECUTE PROCEDURE process_saber_audit();
~~~

