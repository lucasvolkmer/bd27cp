DROP TABLE IF EXISTS Saber CASCADE;
DROP TABLE IF EXISTS Personagem CASCADE;
DROP TABLE IF EXISTS Varinha CASCADE;
DROP TABLE IF EXISTS Casa CASCADE;
DROP TABLE IF EXISTS Feitico CASCADE;

CREATE TABLE IF NOT EXISTS Personagem(
	personagemPK integer PRIMARY KEY,
	nomePersonagem varchar(50) NOT NULL,
	dataNasc date,
	pontosAcumulados integer,
	casaFK integer
);
CREATE TABLE IF NOT EXISTS Casa(
	casaPK integer PRIMARY KEY,
	nomeCasa varchar(15) NOT NULL,
	pontos integer NOT NULL,
	descricao varchar(200), 
	fundadorFK integer REFERENCES Personagem(personagemPK)
);
ALTER TABLE Personagem ADD FOREIGN KEY (casaFK) REFERENCES Casa(casaPK);

CREATE TABLE IF NOT EXISTS Varinha(
	varinhaPK integer PRIMARY KEY,
	material varchar(20) NOT NULL,
	nucleo varchar(20) NOT NULL,
	medida float,
	flex varchar(20),
	donoFK integer REFERENCES Personagem(personagemPK)
);
CREATE TABLE IF NOT EXISTS Feitico(
	feiticoPK integer PRIMARY KEY,
	nomeFeitico varchar(50) NOT NULL,
	descricao varchar(200),
	alcance integer
);
CREATE TABLE IF NOT EXISTS Saber(
	feiticoFK integer REFERENCES Feitico(feiticoPK),
	personagemFK integer REFERENCES Personagem(personagemPK),
	PRIMARY KEY (feiticoFK, personagemFK),
	dataAprendeu date
);


INSERT INTO Casa VALUES (1, 'Corvinal', 0, 'A casa dos que tem a mente sempre alerta,
Onde os homens de grande espírito e saber
Sempre encontrarão companheiros seus iguais', null);
INSERT INTO Casa VALUES (2, 'Grifinória', 0, 'Casa onde habitam os corações indômitos;
Ousadia e sangue-frio e nobreza Destacam os alunos da Grifinória dos demais', null);
INSERT INTO Casa VALUES (3, 'Lufa-Lufa', 0, 'Onde seus moradores são justos e leais,
Pacientes, sinceros, sem medo da dor', null);
INSERT INTO Casa VALUES (4, 'Sonserina', 0, 'E ali fará seus verdadeiros amigos,
Homens de astúcia que usam quaisquer meios para atingir os fins que antes colimaram', null);

INSERT INTO Personagem VALUES (1, 'Harry James Potter', '31-7-1980', 0, 2);
INSERT INTO Personagem VALUES (2, 'Ronald Bilius Weasley', '1-3-1980', 0, 2);
INSERT INTO Personagem VALUES (3, 'Hermione Jean Granger', '19-9-1980', 0, 2);
INSERT INTO Personagem VALUES (4, 'Albus Percival Wulfric Brian Dumbledore', '1-8-1881', 0, 2);
INSERT INTO Personagem VALUES (5, 'Rubeus Hagrid', '6-12-1928', 0, 2);
INSERT INTO Personagem VALUES (6, 'Neville Longbottom', '30-7-1980', 0, 2);
INSERT INTO Personagem VALUES (7, 'Fred Weasley', '1-4-1978', 0, 2);
INSERT INTO Personagem VALUES (8, 'George Weasley', '1-4-1978', 0, 2);
INSERT INTO Personagem VALUES (9, 'Ginevra (Ginny) Molly Weasley', '11-8-1981', 0, 2);
INSERT INTO Personagem VALUES (10, 'Dean Thomas', '1-9-1979', 0, 2);
INSERT INTO Personagem VALUES (11, 'Seamus Finnigan', '1-9-1979', 0, 2);
INSERT INTO Personagem VALUES (12, 'Lily J. Potter', '30-1-1960', 0, 2);
INSERT INTO Personagem VALUES (13, 'James Potter', '27-3-1960', 0, 2);
INSERT INTO Personagem VALUES (14, 'Sirius Black', '3-11-1960', 0, 2);
INSERT INTO Personagem VALUES (15, 'Remus John Lupin', '10-3-1960', 0, 2);
INSERT INTO Personagem VALUES (16, 'Peter Pettigrew', '1-9-1960', 0, 2);
INSERT INTO Personagem VALUES (17, 'Percy Ignatius Weasley', '22-8-1976', 0, 2);
INSERT INTO Personagem VALUES (18, '(Bill) William Arthur Weasley', '29-11-1970', 0, 2);
INSERT INTO Personagem VALUES (19, 'Charles Weasley', '12-12-1972', 0, 2);
INSERT INTO Personagem VALUES (20, 'Lee Jordan ', '1-1-1978', 0, 2);
INSERT INTO Personagem VALUES (21, 'Oliver Wood', '1-1-1976', 0, 2);
INSERT INTO Personagem VALUES (22, 'Angelina Johnson', '24-10-1977', 0, 2);
INSERT INTO Personagem VALUES (23, 'Katie Bell', '1-1-1979', 0, 2);
INSERT INTO Personagem VALUES (24, 'Alicia Spinnet', '1-1-1978', 0, 2);
INSERT INTO Personagem VALUES (25, 'Lavender Brown', '1-1-1980', 0, 2);
INSERT INTO Personagem VALUES (26, 'Parvati Patil', '1-1-1980', 0, 2);
INSERT INTO Personagem VALUES (27, 'Romilda Vane', '1-1-1982', 0, 2);
INSERT INTO Personagem VALUES (28, 'Colin Creevey', '1-1-1981', 0, 2);
INSERT INTO Personagem VALUES (29, 'Cormac McLaggen', '1-1-1979', 0, 2);
INSERT INTO Personagem VALUES (30, 'Minerva McGonagall', '4-10-1935', 0, 2);
INSERT INTO Personagem VALUES (31, 'Molly Weasley', '30-10-1949', 0, 2);
INSERT INTO Personagem VALUES (32, 'Arthur Weasley', '6-2-1950', 0, 2);
INSERT INTO Personagem VALUES (33, 'Quirinus Quirrell', '26-9-1970', 0, 1);
INSERT INTO Personagem VALUES (34, 'Cho Chang', '1-1-1979', 0, 1);
INSERT INTO Personagem VALUES (35, 'Luna Lovegood', '13-2-1981', 0, 1);
INSERT INTO Personagem VALUES (36, 'Gilderoy Lockhart', '26-1-1964', 0, 1);
INSERT INTO Personagem VALUES (37, 'Filius Flitwick', '17-10-1958', 0, 1);
INSERT INTO Personagem VALUES (38, 'Sybill Patricia Trelawney', '9-3-1962', 0, 1);
INSERT INTO Personagem VALUES (39, 'Garrick Ollivander', '25-09-1908', 0, 1);
INSERT INTO Personagem VALUES (40, 'Myrtle Elizabeth Warren (Moaning Myrtle)', '14-6-1928', 0, 1);
INSERT INTO Personagem VALUES (41, 'Padma Patil', '1-1-1980', 0, 1);
INSERT INTO Personagem VALUES (42, 'Michael Corner', '1-1-1980', 0, 1);
INSERT INTO Personagem VALUES (43, 'Marietta Edgecombe', '1-1-1979', 0, 1);
INSERT INTO Personagem VALUES (44, 'Terry Boot', '1-1-1980', 0, 1);
INSERT INTO Personagem VALUES (45, 'Anthony Goldstein', '1-1-1980', 0, 1);
INSERT INTO Personagem VALUES (46, 'Severus Snape', '9-1-1960', 0, 4);
INSERT INTO Personagem VALUES (47, 'Draco Malfoy', '5-6-1980', 0, 4);
INSERT INTO Personagem VALUES (48, 'Vincent Crabbe', '1-1-1980', 0, 4);
INSERT INTO Personagem VALUES (49, 'Gregory Goyle', '1-1-1980', 0, 4);
INSERT INTO Personagem VALUES (50, 'Bellatrix Lestrange', '5-4-1951', 0, 4);
INSERT INTO Personagem VALUES (51, 'Dolores Jane Umbridge', '26-8-1965', 0, 4);
INSERT INTO Personagem VALUES (52, 'Horace Eugene Flaccus Slughorn', '28-4-1882', 0, 4);
INSERT INTO Personagem VALUES (53, 'Lucius Malfoy', '1-1-1954', 0, 4);
INSERT INTO Personagem VALUES (54, 'Narcissa Malfoy', '1-1-1955', 0, 4);
INSERT INTO Personagem VALUES (55, 'Regulus Arcturus Black', '1-1-1961', 0, 4);
INSERT INTO Personagem VALUES (56, 'Pansy Parkinson', '1-1-1981', 0, 4);
INSERT INTO Personagem VALUES (57, 'Blaise Zabini', '1-1-1980', 0, 4);
INSERT INTO Personagem VALUES (58, 'Tom Marvolo Riddle', '31-12-1926', 0, 4);
INSERT INTO Personagem VALUES (59, 'Theodore Nott', '1-1-1980', 0, 4);
INSERT INTO Personagem VALUES (60, 'Rodolphus Lestrange', '1-1-1964', 0, 4);
INSERT INTO Personagem VALUES (61, 'Millicent Bulstrode', '1-1-1980', 0, 4);
INSERT INTO Personagem VALUES (62, 'Graham Montague', '1-1-1981', 0, 4);
INSERT INTO Personagem VALUES (63, 'Bloody Baron', '1-1-982', 0, 4);
INSERT INTO Personagem VALUES (64, 'Marcus Flint', '1-1-1976', 0, 4);
INSERT INTO Personagem VALUES (65, 'Penelope Clearwater', '1-1-1976', 0, 1);
INSERT INTO Personagem VALUES (66, 'Roger Davies', '1-1-1978', 0, 1);
INSERT INTO Personagem VALUES (67, 'Marcus Belby', '1-1-1979', 0, 1);
INSERT INTO Personagem VALUES (68, 'Salazar Slytherin', '1-1-993', 0, 4);
INSERT INTO Personagem VALUES (69, 'Godric Gryffindor', '1-1-976', 0, 2);
INSERT INTO Personagem VALUES (70, 'Rowena Ravenclaw', '1-1-976', 0, 1);
INSERT INTO Personagem VALUES (71, 'Nicholas de Mimsy-Porpington', '1-1-1401', 0, 2);
INSERT INTO Personagem VALUES (72, 'Cuthbert Binns', null, 0, null);
INSERT INTO Personagem VALUES (73, 'Barty Crouch Jr.', '1-1-1962', 0, null);
INSERT INTO Personagem VALUES (74, 'Charity Burbage', '1-1-1976', 0, null);
INSERT INTO Personagem VALUES (75, 'Firenze', null, 0, null);
INSERT INTO Personagem VALUES (76, 'Alecto Carrow', '1-1-1981', 0, 4);
INSERT INTO Personagem VALUES (77, 'Amycus Carrow', '1-1-1982', 0, 4);
INSERT INTO Personagem VALUES (78, 'Helga Hufflepuff', '1-1-976', 0, 3);
INSERT INTO Personagem VALUES (79, 'Fat Friar', '1-1-901', 0, 3);
INSERT INTO Personagem VALUES (80, 'Helena Ravenclaw', '1-1-982', 0, 1);
INSERT INTO Personagem VALUES (81, 'Nymphadora Tonks', '1-1-1973', 0, 3);
INSERT INTO Personagem VALUES (82, 'Pomona Sprout', null, 0, 3);
INSERT INTO Personagem VALUES (83, 'Newton Scamander', '24-2-1897', 0, 3);
INSERT INTO Personagem VALUES (84, 'Cedric Diggory', '1-1-1977', 0, 3);
INSERT INTO Personagem VALUES (85, 'Justin Finch-Fletchley', '1-1-1980', 0, 3);
INSERT INTO Personagem VALUES (86, 'Zacharias Smith', '1-1-1981', 0, 3);
INSERT INTO Personagem VALUES (87, 'Hannah Abbott', '1-1-1980', 0, 3);
INSERT INTO Personagem VALUES (88, 'Ernest Macmillan', '21-4-1980', 0, 3);
INSERT INTO Personagem VALUES (89, 'Susan Bones', '1-1-1980', 0, 3);
INSERT INTO Personagem VALUES (90, 'Walden Macnair', '1-1-1970', 0, 4);
INSERT INTO Personagem VALUES (91, 'Augustus Rookwood', '1-1-1964', 0, 4);
INSERT INTO Personagem VALUES (92, 'Antonin Dolohov', '1-1-1950', 0, 4);
INSERT INTO Personagem VALUES (93, 'Corban Yaxley', '1-1-1970', 0, 4);
INSERT INTO Personagem VALUES (94, 'Igor Karkaroff', '1-1-1965', 0, null);
INSERT INTO Personagem VALUES (95, 'Kingsley Shacklebolt', '1-1-1973', 0, null);
INSERT INTO Personagem VALUES (96, 'Alastor Moody', '8-3-1911', 0, null);
INSERT INTO Personagem VALUES (97, 'Alice Longbottom', null, 0, null);
INSERT INTO Personagem VALUES (98, 'Frank Longbottom', '1-9-1957', 0, 2);
INSERT INTO Personagem VALUES (99, 'Rufus Scrimgeour', '1-1-1968', 0, null);
INSERT INTO Personagem VALUES (100, 'Cornelius Oswald Fudge', '1-1-1964', 0, null);
INSERT INTO Personagem VALUES (101, 'Barty Crouch Sr.', '1-1-1962', 0, null);
INSERT INTO Personagem VALUES (102, 'Amos Diggory', null, 0, null);
INSERT INTO Personagem VALUES (103, 'Dedalus Diggle', '1-1-1964', 0, null);
INSERT INTO Personagem VALUES (104, 'Elphias Doge', '1-1-1881', 0, null);
INSERT INTO Personagem VALUES (105, 'Fleur Isabelle Delacour', '1-1-1977', 0, null);
INSERT INTO Personagem VALUES (106, 'Aberforth Dumbledore', '1-1-1883', 0, null);
INSERT INTO Personagem VALUES (107, 'Mundungus Fletcher', '1-1-1962', 0, null);
INSERT INTO Personagem VALUES (108, 'Sturgis Podmore', '1-1-1957', 0, null);
INSERT INTO Personagem VALUES (109, 'Hestia Jones', '1-1-1978', 0, null);
INSERT INTO Personagem VALUES (110, 'Marlene McKinnon', null, 0, null);
INSERT INTO Personagem VALUES (111, 'Fabian Prewett', null, 0, null);
INSERT INTO Personagem VALUES (112, 'Gideon Prewett', null, 0, null);
INSERT INTO Personagem VALUES (113, 'Emmeline Vance', '1-1-1964', 0, null);
INSERT INTO Personagem VALUES (114, 'Edgar Bones', null, 0, 3);
INSERT INTO Personagem VALUES (115, 'Dorcas Meadowes', null, 0, null);
INSERT INTO Personagem VALUES (116, 'Benjy Fenwick', null, 0, null);
INSERT INTO Personagem VALUES (117, 'Madame Olympe Maxime', null, 0, null);
INSERT INTO Personagem VALUES (118, 'Gabrielle Delacour', '1-1-1986', 0, null);
INSERT INTO Personagem VALUES (119, 'Viktor Krum', '1-4-1976', 0, null);
INSERT INTO Personagem VALUES (120, 'Petunia Dursley', '1-1-1960', 0, null);
INSERT INTO Personagem VALUES (121, 'Vernon Dursley', '1-1-1954', 0, null);
INSERT INTO Personagem VALUES (122, 'Dudley Dursley', '23-6-1980', 0, null);
INSERT INTO Personagem VALUES (123, 'Marge Dursley', '1-1-1954', 0, null);
INSERT INTO Personagem VALUES (124, 'Dennis Creevey', '1-1-1983', 0, 2);
INSERT INTO Personagem VALUES (125, 'Albus Severus Potter', '1-1-2006', 0, 4);
INSERT INTO Personagem VALUES (126, 'Scorpius Hyperion Malfoy', '1-1-2007', 0, 4);
INSERT INTO Personagem VALUES (127, 'Edward Remus Lupin', '1-4-1998', 0, 3);
INSERT INTO Personagem VALUES (128, 'James Sirius Potter', '1-1-2004', 0, 2);
INSERT INTO Personagem VALUES (129, 'Rose Granger-Weasley', '1-1-2006', 0, 2);
INSERT INTO Personagem VALUES (130, 'Argus Filch', '1-1-1951', 0, null);
INSERT INTO Personagem VALUES (131, 'Poppy Pomfrey', '1-1-1954', 0, null);
INSERT INTO Personagem VALUES (132, 'Rolanda Hooch', '1-1-1918', 0, null);
INSERT INTO Personagem VALUES (133, 'Irma Pince', '1-1-1966', 0, null);
INSERT INTO Personagem VALUES (134, 'Aurora Sinistra', '1-1-1968', 0, null);
INSERT INTO Personagem VALUES (135, 'Septima Vector', '1-1-1974', 0, null);
INSERT INTO Personagem VALUES (136, 'Wilhelmina Grubbly-Plank', null, 0, null);
INSERT INTO Personagem VALUES (137, 'Fenrir Greyback', '1-1-1945', 0, null);
INSERT INTO Personagem VALUES (138, 'Gellert Grindelwald', '1-1-1883', 0, null);
INSERT INTO Personagem VALUES (139, 'Dobby', null, 0, null);
INSERT INTO Personagem VALUES (140, 'Monstro', null, 0, null);

--70,69, 78, 68
UPDATE Casa SET fundadorFK = 70 WHERE casaPK = 1;
UPDATE Casa SET fundadorFK = 69 WHERE casaPK = 2;
UPDATE Casa SET fundadorFK = 78 WHERE casaPK = 3;
UPDATE Casa SET fundadorFK = 68 WHERE casaPK = 4;

INSERT INTO Feitico VALUES ( 0, 'Summoning Charm', 'Summons an object', 8);
INSERT INTO Feitico VALUES ( 1, 'Age Line', 'Prevents people above or below a certain age from access to a target', 8);
INSERT INTO Feitico VALUES ( 2, 'Water-Making Spell', 'Conjures water', 2);
INSERT INTO Feitico VALUES ( 3, 'Launch an object up into the air', 'Rockets target upward', 9);
INSERT INTO Feitico VALUES ( 4, 'Albus Dumbledore s Forceful Spell', 'Great Force', 3);
INSERT INTO Feitico VALUES ( 5, 'Unlocking Charm', 'Unlocks target', 3);
INSERT INTO Feitico VALUES ( 6, 'Healing Spell', 'Clears airway', 7);
INSERT INTO Feitico VALUES ( 7, 'Hex that grows antlers on the head', 'Grows antlers on head', 5);
INSERT INTO Feitico VALUES ( 8, 'Anti-Cheating Spell', 'Prevents cheating', 9);
INSERT INTO Feitico VALUES ( 9, 'Anti-Disapparition Jinx', 'Prevents Disapparition within a certain area', 5);
INSERT INTO Feitico VALUES ( 10, 'Anti-intruder jinx', 'Repels intruders', 10);
INSERT INTO Feitico VALUES ( 11, 'Antonin Dolohov s curse', 'Injuries capable of killing', 10);
INSERT INTO Feitico VALUES ( 12, 'Revealing Charm', 'Reveals hidden writing', 10);
INSERT INTO Feitico VALUES ( 13, 'Tracking spell', 'Reveals traces of magic, including footprints and track marks', 4);
INSERT INTO Feitico VALUES ( 14, 'Apparition', 'Magically transports the caster', 4);
INSERT INTO Feitico VALUES ( 15, 'Aqua Eructo Charm', 'Jet of water', 3);
INSERT INTO Feitico VALUES ( 16, 'Spider repelling spell', 'Repels spiders', 2);
INSERT INTO Feitico VALUES ( 17, 'Slowing Charm', 'Slows or stops target s velocity', 7);
INSERT INTO Feitico VALUES ( 18, 'Arrow-shooting spell', 'Fires arrows from the caster s wand', 8);
INSERT INTO Feitico VALUES ( 19, 'Ascendio', 'Lifts caster', 1);
INSERT INTO Feitico VALUES ( 20, 'Killing Curse', 'Instantaneous death', 7);
INSERT INTO Feitico VALUES ( 21, 'Avifors Spell', 'Transforms the target into a bird.', 9);
INSERT INTO Feitico VALUES ( 22, 'Avenseguim', 'Turns object into tracking device', 3);
INSERT INTO Feitico VALUES ( 23, 'Bird-Conjuring Charm', 'Conjures birds', 3);
INSERT INTO Feitico VALUES ( 24, 'Babbling Curse', 'Uncontrollable babbling', 9);
INSERT INTO Feitico VALUES ( 25, 'Badgering', 'Turns target human into a badger', 2);
INSERT INTO Feitico VALUES ( 26, 'Bat-Bogey Hex', 'Transforms target s bogeys into bats', 2);
INSERT INTO Feitico VALUES ( 27, 'Baubillious', 'Jet of white sparks', 10);
INSERT INTO Feitico VALUES ( 28, 'Bedazzling Hex', 'Disguises things', 10);
INSERT INTO Feitico VALUES ( 29, 'Bewitched Snowballs', 'Charms snowballs to follow and harass a designated target', 2);
INSERT INTO Feitico VALUES ( 30, 'Bluebell Flames', 'Conjures bluebell flames', 9);
INSERT INTO Feitico VALUES ( 31, 'Blue sparks', 'Jet of blue sparks', 3);
INSERT INTO Feitico VALUES ( 32, 'Exploding Charm', 'Small explosion', 5);
INSERT INTO Feitico VALUES ( 33, 'Exploding Charm 2', 'Powerful explosion', 6);
INSERT INTO Feitico VALUES ( 34, 'Brackium Emendo', 'Mends bones', 2);
INSERT INTO Feitico VALUES ( 35, 'Bravery Charm', 'Enhances teammates against foes', 8);
INSERT INTO Feitico VALUES ( 36, 'Bridge-conjuring spell', 'Conjures bridges', 10);
INSERT INTO Feitico VALUES ( 37, 'Broom jinx', 'Makes a broomstick attempt to buck its rider off', 8);
INSERT INTO Feitico VALUES ( 38, 'Bubble-Head Charm', 'Creates bubble protecting the head', 8);
INSERT INTO Feitico VALUES ( 39, 'Bubble-producing spell', 'Creates a stream of non-bursting bubbles', 1);
INSERT INTO Feitico VALUES ( 40, 'Hair Loss Curse', 'Removes victim s hair', 2);
INSERT INTO Feitico VALUES ( 41, 'Cantis', 'Makes target sing', 4);
INSERT INTO Feitico VALUES ( 42, 'Extension Charm', 'Increases interior space', 3);
INSERT INTO Feitico VALUES ( 43, 'Seize and Pull Charm', 'Pull caster towards object, Pull object towards caster', 5);
INSERT INTO Feitico VALUES ( 44, 'Cascading Jinx', 'Attacks multiple enemies', 8);
INSERT INTO Feitico VALUES ( 45, 'Caterwauling Charm', 'Sets off a high-pitched shriek if entered', 4);
INSERT INTO Feitico VALUES ( 46, 'Cauldron to Sieve', 'Turns a cauldron into a sieve', 2);
INSERT INTO Feitico VALUES ( 47, 'Cauldron to badger', 'Transforms a cauldron into a badger.', 3);
INSERT INTO Feitico VALUES ( 48, 'Cave inimicum', 'Concealment', 2);
INSERT INTO Feitico VALUES ( 49, 'Cheering Charm', 'Creates joy', 5);
INSERT INTO Feitico VALUES ( 50, 'Circumrota', 'Rotates an object', 4);
INSERT INTO Feitico VALUES ( 51, 'Cistem Aperio', 'Opens chests', 10);
INSERT INTO Feitico VALUES ( 52, 'Locking Spell', 'Locks doors', 2);
INSERT INTO Feitico VALUES ( 53, 'Stickfast Hex', 'Sticks shoes to floor', 4);
INSERT INTO Feitico VALUES ( 54, 'Colour Change Charm', 'Changes colour', 6);
INSERT INTO Feitico VALUES ( 55, 'Blasting Curse', 'Explosion', 1);
INSERT INTO Feitico VALUES ( 56, 'Confundus Charm', 'Confuses the target', 3);
INSERT INTO Feitico VALUES ( 57, 'Conjunctivitis Curse', 'Irritates eyes', 4);
INSERT INTO Feitico VALUES ( 58, 'Cornflake skin spell', 'Makes skin appear like cornflakes', 1);
INSERT INTO Feitico VALUES ( 59, 'Cracker Jinx', 'Conjures exploding Wizard Crackers', 1);
INSERT INTO Feitico VALUES ( 60, 'Cribbing Spell', 'Assists the caster in cheating on written papers.', 2);
INSERT INTO Feitico VALUES ( 61, 'Crinus Muto', 'Transforms hair', 10);
INSERT INTO Feitico VALUES ( 62, 'Cruciatus Curse', 'Excruciating pain', 4);
INSERT INTO Feitico VALUES ( 63, 'Gouging Spell', 'Carves through material', 3);
INSERT INTO Feitico VALUES ( 64, 'Eradication Spell', 'Erases image from Reverse Spell', 1);
INSERT INTO Feitico VALUES ( 65, 'Densaugeo', 'Elongates teeth', 5);
INSERT INTO Feitico VALUES ( 66, 'Deprimo', 'Blasts holes in ground', 8);
INSERT INTO Feitico VALUES ( 67, 'Banishing Charm', 'Sends target away', 1);
INSERT INTO Feitico VALUES ( 68, 'Descendo', 'Lowers target', 1);
INSERT INTO Feitico VALUES ( 69, 'Desk Into Pig', 'Transfigures desks into pigs', 3);
INSERT INTO Feitico VALUES ( 70, 'Deterioration Hex', 'Impairs foes', 2);
INSERT INTO Feitico VALUES ( 71, 'Severing Charm', 'Cuts objects', 4);
INSERT INTO Feitico VALUES ( 72, 'Diminuendo', 'Shrinks objects', 1);
INSERT INTO Feitico VALUES ( 73, 'Dissendium', 'Reveals secret passages', 8);
INSERT INTO Feitico VALUES ( 74, 'Disillusionment Charm', 'Disguises target as surroundings', 8);
INSERT INTO Feitico VALUES ( 75, 'Draconifors Spell', 'Turns object into dragon', 10);
INSERT INTO Feitico VALUES ( 76, 'Drought Charm', 'Dries up small bodies of water', 3);
INSERT INTO Feitico VALUES ( 77, 'Ducklifors Jinx', 'Turns organisms to ducks', 10);
INSERT INTO Feitico VALUES ( 78, 'Hardening Charm', 'Turns object to stone', 3);
INSERT INTO Feitico VALUES ( 79, 'Ears to kumquats', 'Transfigures target s ears into kumquats', 2);
INSERT INTO Feitico VALUES ( 80, 'Ear-shrivelling Curse', 'Causes ears to wither', 1);
INSERT INTO Feitico VALUES ( 81, 'Ebublio Jinx', 'Traps target in giant bubble', 8);
INSERT INTO Feitico VALUES ( 82, 'Engorgement Charm', 'Causes swelling', 6);
INSERT INTO Feitico VALUES ( 83, 'Engorgio Skullus', 'Swells head', 1);
INSERT INTO Feitico VALUES ( 84, 'Insect Jinx', 'Turns into insect', 9);
INSERT INTO Feitico VALUES ( 85, 'Entrail-Expelling Curse', 'Expels entrails', 8);
INSERT INTO Feitico VALUES ( 86, 'Epoximise', 'Bonds two objects', 3);
INSERT INTO Feitico VALUES ( 87, 'Erecto', 'Erects a structure', 2);
INSERT INTO Feitico VALUES ( 88, 'Evanesce', 'Vanishes objects', 4);
INSERT INTO Feitico VALUES ( 89, 'Vanishing Spell', 'Vanishes things', 9);
INSERT INTO Feitico VALUES ( 90, 'Everte Statum', 'Makes opponent stumble, Causes sharp pain', 9);
INSERT INTO Feitico VALUES ( 91, 'Patronus Charm', 'Conjures a spirit guardian', 7);
INSERT INTO Feitico VALUES ( 92, 'Disarming Charm', 'Disarms an opponent', 4);
INSERT INTO Feitico VALUES ( 93, 'Expulso Curse', 'Blows things up', 3);
INSERT INTO Feitico VALUES ( 94, 'Extinguishing Spell', 'Extinguishes fires', 9);
INSERT INTO Feitico VALUES ( 95, 'False memory charm', 'Implants a false memory in the victim', 6);
INSERT INTO Feitico VALUES ( 96, 'Feather-light charm', 'Minimises object s weight', 6);
INSERT INTO Feitico VALUES ( 97, 'Ferret to human', 'Changes ferrets into humans', 9);
INSERT INTO Feitico VALUES ( 98, 'Bandaging Charm', 'Bandages target', 2);
INSERT INTO Feitico VALUES ( 99, 'Fianto Duri', 'Most likely hardens magical shields', 7);
INSERT INTO Feitico VALUES ( 100, 'Fidelius Charm', 'Conceals a secret', 4);
INSERT INTO Feitico VALUES ( 101, 'Fiendfyre', 'Unleashes cursed fire', 2);
INSERT INTO Feitico VALUES ( 102, 'Finestra spell', 'Shatters glass', 1);
INSERT INTO Feitico VALUES ( 103, 'General Counter-Spell', 'Terminates all spell effects', 9);
INSERT INTO Feitico VALUES ( 104, 'Finger-removing jinx', 'Removes a person s fingers', 7);
INSERT INTO Feitico VALUES ( 105, 'Firestorm', 'Ring of fire', 4);
INSERT INTO Feitico VALUES ( 106, 'Flagrante Curse', 'Causes objects to burn on contact', 4);
INSERT INTO Feitico VALUES ( 107, 'Flagrate', 'Writes in midair', 8);
INSERT INTO Feitico VALUES ( 108, 'Flame-Freezing Charm', 'Makes fire harmless', 6);
INSERT INTO Feitico VALUES ( 109, 'Flask-conjuring spell', 'Conjures flask', 3);
INSERT INTO Feitico VALUES ( 110, 'Flintifors', 'Turns target into matchboxes', 6);
INSERT INTO Feitico VALUES ( 111, 'Knockback Jinx', 'Knocks target back', 8);
INSERT INTO Feitico VALUES ( 112, 'Knockback Jinx Duo', 'Knocks target back', 7);
INSERT INTO Feitico VALUES ( 113, 'Flipendo Tria', 'More powerful version of Flipendo', 3);
INSERT INTO Feitico VALUES ( 114, 'Flying Charm', 'Allows an object to fly', 7);
INSERT INTO Feitico VALUES ( 115, 'Smokescreen Spell', 'Defensive smokescreen', 6);
INSERT INTO Feitico VALUES ( 116, 'Fumos Duo', 'Multiple concealing smokescreens', 9);
INSERT INTO Feitico VALUES ( 117, 'Pimple Jinx', 'Causes pimples to erupt', 5);
INSERT INTO Feitico VALUES ( 118, 'Fur spell', 'Causes fur to grow on someone', 2);
INSERT INTO Feitico VALUES ( 119, 'Doubling Charm', 'Duplicates an object', 8);
INSERT INTO Feitico VALUES ( 120, 'Freezing Spell', 'Freezes target', 4);
INSERT INTO Feitico VALUES ( 121, 'Glacius Duo', 'Freezes the target', 8);
INSERT INTO Feitico VALUES ( 122, 'Glacius Tria', 'Freezes target enemy', 2);
INSERT INTO Feitico VALUES ( 123, 'Glisseo', 'Turns stairs into a slide', 4);
INSERT INTO Feitico VALUES ( 124, 'Green Sparks', 'Jet of green sparks', 9);
INSERT INTO Feitico VALUES ( 125, 'Gripping Charm', 'Makes holding easier', 9);
INSERT INTO Feitico VALUES ( 126, 'Hair-thickening Charm', 'Causes hair to grow longer and thicker', 4);
INSERT INTO Feitico VALUES ( 127, 'Harmonia Nectere Passus', 'Repairs Vanishing Cabinets', 2);
INSERT INTO Feitico VALUES ( 128, 'Herbifors Spell', 'Transforms target into flowers', 9);
INSERT INTO Feitico VALUES ( 129, 'Herbivicus Charm', 'Rapidly grows plants', 5);
INSERT INTO Feitico VALUES ( 130, 'Dumbledore s Army parchment jinx', 'Causes boils to spell "SNEAK" on the face of a traitor', 1);
INSERT INTO Feitico VALUES ( 131, 'Homing spell', 'Offensive spells; will follow their target', 7);
INSERT INTO Feitico VALUES ( 132, 'Human Presence Revealing Spell', 'Reveals human presence', 7);
INSERT INTO Feitico VALUES ( 133, 'Homonculous Charm', 'When cast onto a map, it enables to track the movements of every person in the mapped area', 10);
INSERT INTO Feitico VALUES ( 134, 'Homorphus Charm', 'May temporarily change a transformed werewolf back into their human form (according to Gilderoy Lockhart, it cures lycanthropy)', 9);
INSERT INTO Feitico VALUES ( 135, 'Horn tongue hex', 'Transforms target s tongue into a horn', 5);
INSERT INTO Feitico VALUES ( 136, 'Horton-Keitch Braking Charm', 'Allow broomsticks to brake more easily', 4);
INSERT INTO Feitico VALUES ( 137, 'Horcrux-making spell', 'Prepared a receptacle to become a Horcrux', 6);
INSERT INTO Feitico VALUES ( 138, 'Hot-Air Charm', 'Jet of hot air', 5);
INSERT INTO Feitico VALUES ( 139, 'Hour-Reversal Charm', 'Reverses time', 3);
INSERT INTO Feitico VALUES ( 140, 'Hover Charm', 'Makes objects hover', 2);
INSERT INTO Feitico VALUES ( 141, 'Hurling Hex', 'Unclear, possibly causes a broomstick to attempt to hurl its rider off.', 3);
INSERT INTO Feitico VALUES ( 142, 'Illegibilus', 'Makes text illegible', 3);
INSERT INTO Feitico VALUES ( 143, 'Freezing Charm', 'Stops movement and actions of the target', 2);
INSERT INTO Feitico VALUES ( 144, 'Impediment Jinx', 'Hinders movement', 1);
INSERT INTO Feitico VALUES ( 145, 'Imperius Curse', 'Total control over the victim', 3);
INSERT INTO Feitico VALUES ( 146, 'Imperturbable Charm', 'Invisible barrier', 9);
INSERT INTO Feitico VALUES ( 147, 'Impervius Charm', 'Waterproofs an object', 2);
INSERT INTO Feitico VALUES ( 148, 'Inanimatus Conjurus Spell', 'Presumably conjuresinanimate objects', 9);
INSERT INTO Feitico VALUES ( 149, 'Incarcerous Spell', 'Binds target in ropes', 9);
INSERT INTO Feitico VALUES ( 150, 'Fire-Making Spell', 'Conjures flames', 3);
INSERT INTO Feitico VALUES ( 151, 'Incendio Duo Spell', 'Conjures flames', 4);
INSERT INTO Feitico VALUES ( 152, 'Incendio Tria', 'Conjures blue flames', 6);
INSERT INTO Feitico VALUES ( 153, 'Inflating Charm', 'Inflates target', 8);
INSERT INTO Feitico VALUES ( 154, 'Informous Spell', 'Adds to the Folio Bruti', 4);
INSERT INTO Feitico VALUES ( 155, 'Instant Scalping Hex', 'Instantly scalps hair', 2);
INSERT INTO Feitico VALUES ( 156, 'Intruder Charm', 'Detects intruders and sounds an alarm', 4);
INSERT INTO Feitico VALUES ( 157, 'Jelly-Legs Curse', 'Legs become wobbly', 3);
INSERT INTO Feitico VALUES ( 158, 'Jelly-Brain Jinx', 'Dulls mental abilities', 3);
INSERT INTO Feitico VALUES ( 159, 'Jelly-Fingers Curse', 'Fingers become wobbly', 10);
INSERT INTO Feitico VALUES ( 160, 'Knee-reversal hex', 'Puts knees on backward', 4);
INSERT INTO Feitico VALUES ( 161, 'Lacarnum Inflamari', 'Ignites cloaks', 1);
INSERT INTO Feitico VALUES ( 162, 'Langlock', 'Sticks tongue to roof of the mouth', 5);
INSERT INTO Feitico VALUES ( 163, 'Lapifors Spell', 'Turns target into rabbit', 7);
INSERT INTO Feitico VALUES ( 164, 'Leek jinx', 'Leeks sprout from ears', 3);
INSERT INTO Feitico VALUES ( 165, 'Legilimency Spell', 'Lets caster see into the mind of another person', 4);
INSERT INTO Feitico VALUES ( 166, 'Levicorpus', 'Suspends people by the ankles', 6);
INSERT INTO Feitico VALUES ( 167, 'Liberacorpus', 'Counteracts Levicorpus', 8);
INSERT INTO Feitico VALUES ( 168, 'Locomotion Charm', 'Moves objects in midair', 9);
INSERT INTO Feitico VALUES ( 169, 'Leg-Locker Curse', 'Sticks legs together', 10);
INSERT INTO Feitico VALUES ( 170, 'Wand-Lighting Charm', 'Illuminates the wand tip', 8);
INSERT INTO Feitico VALUES ( 171, 'Wand-Lighting Charm Duo', 'Focused beam of light', 10);
INSERT INTO Feitico VALUES ( 172, 'Lumos Maxima', 'Produces bright light', 10);
INSERT INTO Feitico VALUES ( 173, 'Lumos Solem Spell', 'Produces sunlight', 9);
INSERT INTO Feitico VALUES ( 174, 'Magicus Extremos', 'Temporarily increases casters spell power', 2);
INSERT INTO Feitico VALUES ( 175, 'Melofors Jinx', 'Encases head in pumpkin', 5);
INSERT INTO Feitico VALUES ( 176, 'Meteolojinx Recanto', 'Causes weather effects caused by incantations to cease', 4);
INSERT INTO Feitico VALUES ( 177, 'Tongue-Tying Curse', 'Ties tongue in knot', 1);
INSERT INTO Feitico VALUES ( 178, 'Ministry of Magic Fog', 'Produces special fog (for concealment of certain locations)', 7);
INSERT INTO Feitico VALUES ( 179, 'Mobiliarbus', 'Levitates wooden things', 4);
INSERT INTO Feitico VALUES ( 180, 'Mobilicorpus', 'Moves bodies', 4);
INSERT INTO Feitico VALUES ( 181, 'Cushioning Charm', 'Invisible cushion', 4);
INSERT INTO Feitico VALUES ( 182, 'Dark Mark', 'Conjures Dark Mark', 3);
INSERT INTO Feitico VALUES ( 183, 'Curse of the Bogies', 'Nasty cold & runny nose', 9);
INSERT INTO Feitico VALUES ( 184, 'Muffliato Charm', 'Conceals sound', 4);
INSERT INTO Feitico VALUES ( 185, 'Multicorfors Spell', 'Changes clothes', 2);
INSERT INTO Feitico VALUES ( 186, 'Mutatio Skullus', 'Grows extra heads', 5);
INSERT INTO Feitico VALUES ( 187, 'Wand-Extinguishing Charm', 'Extinguishes wandlight', 2);
INSERT INTO Feitico VALUES ( 188, 'Nebulus', 'Conjures fog', 8);
INSERT INTO Feitico VALUES ( 189, 'Oculus Reparo', 'Repairs glasses', 2);
INSERT INTO Feitico VALUES ( 190, 'Obliteration Charm', 'Removes footprints', 3);
INSERT INTO Feitico VALUES ( 191, 'Memory Charm', 'Erases memories', 5);
INSERT INTO Feitico VALUES ( 192, 'Obscuro', 'Blindfolds target', 2);
INSERT INTO Feitico VALUES ( 193, 'Oppugno Jinx', 'Assaults target with directed object(s)', 7);
INSERT INTO Feitico VALUES ( 194, 'Orbis Jinx', 'Sucks the target into the ground', 10);
INSERT INTO Feitico VALUES ( 195, 'Orchideous', 'Conjures flowers', 2);
INSERT INTO Feitico VALUES ( 196, 'Oscausi', 'Seals mouth', 7);
INSERT INTO Feitico VALUES ( 197, 'Pack', 'Packs luggage', 3);
INSERT INTO Feitico VALUES ( 198, 'Papyrus Reparo', 'Mends torn paper', 7);
INSERT INTO Feitico VALUES ( 199, 'Patented Daydream Charm', 'Makes the user daydream for 30 minutes', 3);
INSERT INTO Feitico VALUES ( 200, 'Partis Temporus', 'Parts the target', 3);
INSERT INTO Feitico VALUES ( 201, 'Pepper breath hex', 'Inflicts fiery hot breath', 2);
INSERT INTO Feitico VALUES ( 202, 'Periculum', 'Burst of red sparks', 5);
INSERT INTO Feitico VALUES ( 203, 'Permanent Sticking Charm', 'Sticks object permanently in place', 7);
INSERT INTO Feitico VALUES ( 204, 'Peskipiksi Pesternomi', 'Nothing (supposedly captures pixies)', 9);
INSERT INTO Feitico VALUES ( 205, 'Full Body-Bind Curse', 'Temporary Paralysis', 9);
INSERT INTO Feitico VALUES ( 206, 'Piertotum Locomotor', 'Animates target', 3);
INSERT INTO Feitico VALUES ( 207, 'Piscifors', 'Turns target into fish', 9);
INSERT INTO Feitico VALUES ( 208, 'Placement Charm', 'Places object on target', 8);
INSERT INTO Feitico VALUES ( 209, 'Four-Point Spell', 'Wand points due north', 4);
INSERT INTO Feitico VALUES ( 210, 'Portus', 'Turns object into Portkey', 4);
INSERT INTO Feitico VALUES ( 211, 'Reverse Spell', 'Shows the previous spells cast by a wand', 7);
INSERT INTO Feitico VALUES ( 212, 'Protean Charm', 'Links objects', 6);
INSERT INTO Feitico VALUES ( 213, 'Shield Charm', 'Reflects spells and blocks physical forces', 3);
INSERT INTO Feitico VALUES ( 214, 'Protego Diabolica', 'Ring of protective black fire that only burns the caster s enemies whilst leaving their allies unharmed', 4);
INSERT INTO Feitico VALUES ( 215, 'Protego horribilis', 'Summons a powerful protective barrier', 6);
INSERT INTO Feitico VALUES ( 216, 'Protego Maxima', 'Summons a large protective barrier', 5);
INSERT INTO Feitico VALUES ( 217, 'Protego totalum', 'Shields an area', 8);
INSERT INTO Feitico VALUES ( 218, 'Purple Firecrackers', 'Exploding firecrackers', 4);
INSERT INTO Feitico VALUES ( 219, 'Pus-squirting hex', 'Causes yellowish goo to squirt from one s nose', 9);
INSERT INTO Feitico VALUES ( 220, 'Quietening Charm', 'Quietens target', 5);
INSERT INTO Feitico VALUES ( 221, 'Head Shrink Spell', 'Shrinks head', 1);
INSERT INTO Feitico VALUES ( 222, 'Shrinking Charm', 'Shrinks target', 6);
INSERT INTO Feitico VALUES ( 223, 'Reductor Curse', 'Destroys solid objects', 5);
INSERT INTO Feitico VALUES ( 224, 'Refilling Charm', 'Refills beverage containers', 10);
INSERT INTO Feitico VALUES ( 225, 'Reparifors', 'Heals minor magically induced ailments', 4);
INSERT INTO Feitico VALUES ( 226, 'Reverte', 'Returns objects to their original positions or states', 4);
INSERT INTO Feitico VALUES ( 227, 'Revulsion Jinx', 'Forces target to let go', 7);
INSERT INTO Feitico VALUES ( 228, 'Reviving Spell', 'Awakens victim', 4);
INSERT INTO Feitico VALUES ( 229, 'Reparifarge', 'Reverses the effects of an incomplete Transformation spell', 7);
INSERT INTO Feitico VALUES ( 230, 'Mending Charm', 'Fixes broken objects', 3);
INSERT INTO Feitico VALUES ( 231, 'Muggle-Repelling Charm', 'Repels Muggles', 9);
INSERT INTO Feitico VALUES ( 232, 'Repello Inimicum', 'Repels enemies. If used with Protego Maxima and Fianto Duri, it disintegrates anything that comes in contact with the barrier.', 10);
INSERT INTO Feitico VALUES ( 233, 'Revelio Charm', 'Reveals secrets about a person or object', 7);
INSERT INTO Feitico VALUES ( 234, 'Tickling Charm', 'Tickles and weakens', 3);
INSERT INTO Feitico VALUES ( 235, 'Boggart-Banishing Spell', 'Turns a Boggart into something amusing', 1);
INSERT INTO Feitico VALUES ( 236, 'Rose Growth', 'Accelerates the growth of a rosebush', 4);
INSERT INTO Feitico VALUES ( 237, 'Rowboat spell', 'Causes a rowboat to propel itself.', 3);
INSERT INTO Feitico VALUES ( 238, 'Salvio hexia', 'Hex deflection', 6);
INSERT INTO Feitico VALUES ( 239, 'Sardine hex', 'Victim sneezes sardines', 5);
INSERT INTO Feitico VALUES ( 240, 'Sauce-making spell', 'Conjures sauce', 7);
INSERT INTO Feitico VALUES ( 241, 'Minerva McGonagall s fire-creating spell', 'Creates fire', 6);
INSERT INTO Feitico VALUES ( 242, 'Scouring Charm', 'Cleans objects', 3);
INSERT INTO Feitico VALUES ( 243, 'Sealant Charm', 'Seals envelopes', 1);
INSERT INTO Feitico VALUES ( 244, 'Sea urchin jinx', 'Makes tiny spikes erupt all over the victim', 8);
INSERT INTO Feitico VALUES ( 245, 'Sectumsempra', 'Lacerates target', 8);
INSERT INTO Feitico VALUES ( 246, 'Shield penetration spell', 'Used to break down magical shields', 3);
INSERT INTO Feitico VALUES ( 247, 'Shooting spell', 'Small explosion with a gunshot-sound', 3);
INSERT INTO Feitico VALUES ( 248, 'Smashing spell', 'Explosive', 8);
INSERT INTO Feitico VALUES ( 249, 'Snake Summons Spell', 'Conjures snake', 1);
INSERT INTO Feitico VALUES ( 250, 'Silencing Charm', 'Silences target', 10);
INSERT INTO Feitico VALUES ( 251, 'Skurge Charm', 'Cleans ectoplasm, Frightens ghosts and other spirits', 9);
INSERT INTO Feitico VALUES ( 252, 'Slippery Jinx', 'Makes object slippery', 2);
INSERT INTO Feitico VALUES ( 253, 'Slug-vomiting Charm', 'Victim vomits slugs', 7);
INSERT INTO Feitico VALUES ( 254, 'Sonorous Charm', 'Emits a magnified roar from the tip of the wand', 3);
INSERT INTO Feitico VALUES ( 255, 'Amplifying Charm', 'Loudens target', 3);
INSERT INTO Feitico VALUES ( 256, 'Specialis Revelio', 'Reveals spells cast on objects or potions', 7);
INSERT INTO Feitico VALUES ( 257, 'Sponge-Knees Curse', 'Causes the target s legs to become spongy', 1);
INSERT INTO Feitico VALUES ( 258, 'Softening Charm', 'Softens objects', 3);
INSERT INTO Feitico VALUES ( 259, 'Squiggle Quill', 'Transfigures writing quills into worms', 2);
INSERT INTO Feitico VALUES ( 260, 'Stealth Sensoring Spell', 'Detects those under magicaldisguise.', 7);
INSERT INTO Feitico VALUES ( 261, 'Steleus', 'Causes target to sneeze', 1);
INSERT INTO Feitico VALUES ( 262, 'Stinging Jinx', 'Stings flesh', 7);
INSERT INTO Feitico VALUES ( 263, 'Stretching Jinx', 'Expands the target', 8);
INSERT INTO Feitico VALUES ( 264, 'Stunning Spell', 'Knocks out target', 8);
INSERT INTO Feitico VALUES ( 265, 'Supersensory Charm', 'Superior perception', 4);
INSERT INTO Feitico VALUES ( 266, 'Surgito', 'Removes enchantments', 1);
INSERT INTO Feitico VALUES ( 267, 'Switching Spell', 'Switches two objects', 4);
INSERT INTO Feitico VALUES ( 268, 'Taboo', 'When a word is spoken, the caster is alerted and protective enchantments around the speaker are weakened.', 2);
INSERT INTO Feitico VALUES ( 269, 'Tail-growing hex', 'Gives the victim a tail', 4);
INSERT INTO Feitico VALUES ( 270, 'Dancing Feet Spell', 'Sends legs out of control', 6);
INSERT INTO Feitico VALUES ( 271, 'Teacup to tortoise', 'Transforms a teacup into a tortoise', 9);
INSERT INTO Feitico VALUES ( 272, 'Teapot to tortoise', 'Transforms a teapot into a tortoise', 9);
INSERT INTO Feitico VALUES ( 273, 'Teeth-straightening spell', 'Straightens teeth', 5);
INSERT INTO Feitico VALUES ( 274, 'Teleportation spell', 'Makes objects teleport elsewhere', 2);
INSERT INTO Feitico VALUES ( 275, 'Tentaclifors', 'Turns victim s head into tentacle', 8);
INSERT INTO Feitico VALUES ( 276, 'Tergeo', 'Cleans up objects', 9);
INSERT INTO Feitico VALUES ( 277, 'Tickling Hex', 'Tickles and weakens', 4);
INSERT INTO Feitico VALUES ( 278, 'Toenail-growing hex', 'Causes its victim s toenails to grow alarmingly fast', 4);
INSERT INTO Feitico VALUES ( 279, 'Transmogrifian Torture', 'Tortures, possibly by transfiguring the target to death', 10);
INSERT INTO Feitico VALUES ( 280, 'Trip Jinx', 'Trips victim', 9);
INSERT INTO Feitico VALUES ( 281, 'Twitchy-Ears Hex', 'Makes victim s ears twitch', 5);
INSERT INTO Feitico VALUES ( 282, 'Unbreakable Charm', 'Makes object unbreakable', 6);
INSERT INTO Feitico VALUES ( 283, 'Unbreakable Vow', 'Magical vow that is fatal if broken', 3);
INSERT INTO Feitico VALUES ( 284, 'Unsupported flight', 'Allows a witch or wizard to fly unaided', 5);
INSERT INTO Feitico VALUES ( 285, 'Vacuum cleaner spell', 'Cleans an object like a vacuum cleaner', 6);
INSERT INTO Feitico VALUES ( 286, 'Ventus Jinx', 'Jet of spiralling wind', 3);
INSERT INTO Feitico VALUES ( 287, 'Ventus Duo', 'Creates a stronger jet of wind compared to Ventus', 6);
INSERT INTO Feitico VALUES ( 288, 'Vera Verto', 'Transforms animals into water goblets', 4);
INSERT INTO Feitico VALUES ( 289, 'Verdillious', 'Causes the wand tip to burn like a sparkler whilst damaging the foe.', 5);
INSERT INTO Feitico VALUES ( 290, 'Verdimillious Charm', 'Emits green sparks from the wand that can: Do damage to opponents , Reveal hidden things', 7);
INSERT INTO Feitico VALUES ( 291, 'Verdimillious Duo Spell', 'Emits green sparks from the wand that can: Do damage to opponents , Reveal hidden things', 2);
INSERT INTO Feitico VALUES ( 292, 'Vermiculus Jinx', 'Turns things into worms', 8);
INSERT INTO Feitico VALUES ( 293, 'Red Sparks', 'Jet of red sparks', 10);
INSERT INTO Feitico VALUES ( 294, 'Snake-Vanishing Spell', 'Vanishes snakes', 10);
INSERT INTO Feitico VALUES ( 295, 'Vulnera Sanentur', 'Slows blood flow, Clears residue, Knits wounds', 5);
INSERT INTO Feitico VALUES ( 296, 'Waddiwasi', 'Propels wad at the target', 4);
INSERT INTO Feitico VALUES ( 297, 'Washing up spell', 'Cleans dishes', 7);
INSERT INTO Feitico VALUES ( 298, 'Levitation Charm', 'Makes objects fly', 9);
INSERT INTO Feitico VALUES ( 299, 'White sparks', 'Jet of white sparks', 8);
INSERT INTO Feitico VALUES ( 300, 'Episkey', 'Heals minor injuries', 10);

INSERT INTO Varinha VALUES ( 1, 'Cherry', 'Dragon heartstring', 11.06, 'Little Flexivel', 127);
INSERT INTO Varinha VALUES ( 2, 'Ash', 'Troll whisker', 10.24, 'Very flexible', 97);
INSERT INTO Varinha VALUES ( 3, 'Vine', 'Unicorn hair', 9.39, 'Very flexible', 125);
INSERT INTO Varinha VALUES ( 4, 'Blackthorn', 'Troll whisker', 11.45, 'Very flexible', 129);
INSERT INTO Varinha VALUES ( 5, 'Cypress', 'Dragon heartstring', 9.62, 'Little Flexivel', 27);
INSERT INTO Varinha VALUES ( 6, 'Willow', 'Troll whisker', 7.12, 'Very flexible', 70);
INSERT INTO Varinha VALUES ( 7, 'Hazel', 'Thestral tail hair', 10.17, 'Very flexible', 21);
INSERT INTO Varinha VALUES ( 8, 'Acacia', 'Thestral tail hair', 9.83, 'Little Flexivel', 30);
INSERT INTO Varinha VALUES ( 9, 'Apple', 'Veela hair', 10.96, 'Very flexible', 70);
INSERT INTO Varinha VALUES ( 10, 'Ebony', 'Veela hair', 9.44, 'Little Flexivel', 127);
INSERT INTO Varinha VALUES ( 11, 'Hazel', 'Phoenix feather', 6.56, 'Very flexible', 40);
INSERT INTO Varinha VALUES ( 12, 'Cherry', 'Kelpie hair', 6.01, 'Little Flexivel', 83);
INSERT INTO Varinha VALUES ( 13, 'Ash', 'Veela hair', 11.24, 'Very flexible', 65);
INSERT INTO Varinha VALUES ( 14, 'Apple', 'Unicorn hair', 12.54, 'Very flexible', 39);
INSERT INTO Varinha VALUES ( 15, 'Blackthorn', 'Phoenix feather', 9.72, 'Little Flexivel', 6);
INSERT INTO Varinha VALUES ( 16, 'Ebony', 'Thestral tail hair', 9.19, 'Very flexible', 78);
INSERT INTO Varinha VALUES ( 17, 'Cypress', 'Dragon heartstring', 7.01, 'Little Flexivel', 123);
INSERT INTO Varinha VALUES ( 18, 'Hazel', 'Unicorn hair', 12.12, 'Very flexible', 70);
INSERT INTO Varinha VALUES ( 19, 'Ash', 'Thestral tail hair', 9.24, 'Little Flexivel', 140);
INSERT INTO Varinha VALUES ( 20, 'Acacia', 'Veela hair', 9.73, 'Very flexible', 22);
INSERT INTO Varinha VALUES ( 21, 'Acacia', 'Kelpie hair', 8.78, 'Very flexible', 128);
INSERT INTO Varinha VALUES ( 22, 'Acacia', 'Dragon heartstring', 10.1, 'Very flexible', 8);
INSERT INTO Varinha VALUES ( 23, 'Apple', 'Unicorn hair', 9.67, 'Very flexible', 108);
INSERT INTO Varinha VALUES ( 24, 'Cypress', 'Kelpie hair', 6.01, 'Little Flexivel', 106);
INSERT INTO Varinha VALUES ( 25, 'Acacia', 'Dragon heartstring', 8.06, 'Very flexible', 82);
INSERT INTO Varinha VALUES ( 26, 'Ebony', 'Dragon heartstring', 10.06, 'Little Flexivel', 39);
INSERT INTO Varinha VALUES ( 27, 'Ebony', 'Troll whisker', 8.92, 'Very flexible', 28);
INSERT INTO Varinha VALUES ( 28, 'Cypress', 'Kelpie hair', 12.31, 'Very flexible', 10);
INSERT INTO Varinha VALUES ( 29, 'Ebony', 'Basilisk horn', 11.55, 'Little Flexivel', 126);
INSERT INTO Varinha VALUES ( 30, 'Apple', 'Veela hair', 12.38, 'Very flexible', 132);
INSERT INTO Varinha VALUES ( 31, 'Cherry', 'Unicorn hair', 7.18, 'Very flexible', 21);
INSERT INTO Varinha VALUES ( 32, 'Cherry', 'Dragon heartstring', 8.03, 'Very flexible', 2);
INSERT INTO Varinha VALUES ( 33, 'Willow', 'Troll whisker', 12.38, 'Little Flexivel', 113);
INSERT INTO Varinha VALUES ( 34, 'Maple', 'Unicorn hair', 11.39, 'Very flexible', 10);
INSERT INTO Varinha VALUES ( 35, 'Ebony', 'Basilisk horn', 11.65, 'Very flexible', 69);
INSERT INTO Varinha VALUES ( 36, 'Alder', 'Basilisk horn', 8.83, 'Little Flexivel', 3);
INSERT INTO Varinha VALUES ( 37, 'Ebony', 'Unicorn hair', 11.26, 'Very flexible', 25);
INSERT INTO Varinha VALUES ( 38, 'Cherry', 'Kelpie hair', 6.05, 'Very flexible', 5);
INSERT INTO Varinha VALUES ( 39, 'Cherry', 'Veela hair', 11.91, 'Little Flexivel', 131);
INSERT INTO Varinha VALUES ( 40, 'Ebony', 'Unicorn hair', 11.65, 'Little Flexivel', 81);
INSERT INTO Varinha VALUES ( 41, 'Hazel', 'Veela hair', 10.32, 'Little Flexivel', 41);
INSERT INTO Varinha VALUES ( 42, 'Hazel', 'Troll whisker', 6.25, 'Very flexible', 1);
INSERT INTO Varinha VALUES ( 43, 'Blackthorn', 'Veela hair', 6.96, 'Very flexible', 27);
INSERT INTO Varinha VALUES ( 44, 'Silver lime', 'Veela hair', 10.87, 'Very flexible', 137);
INSERT INTO Varinha VALUES ( 45, 'Maple', 'Unicorn hair', 11.63, 'Little Flexivel', 90);
INSERT INTO Varinha VALUES ( 46, 'Vine', 'Dragon heartstring', 11.39, 'Very flexible', 108);
INSERT INTO Varinha VALUES ( 47, 'Apple', 'Troll whisker', 7.05, 'Little Flexivel', 27);
INSERT INTO Varinha VALUES ( 48, 'Hazel', 'Kelpie hair', 7.68, 'Little Flexivel', 38);
INSERT INTO Varinha VALUES ( 49, 'Alder', 'Troll whisker', 11.61, 'Very flexible', 28);
INSERT INTO Varinha VALUES ( 50, 'Hazel', 'Thestral tail hair', 11.28, 'Very flexible', 137);
INSERT INTO Varinha VALUES ( 51, 'Alder', 'Basilisk horn', 12.72, 'Little Flexivel', 83);
INSERT INTO Varinha VALUES ( 52, 'Maple', 'Kelpie hair', 10.35, 'Very flexible', 123);
INSERT INTO Varinha VALUES ( 53, 'Apple', 'Kelpie hair', 10.89, 'Very flexible', 105);
INSERT INTO Varinha VALUES ( 54, 'Alder', 'Dragon heartstring', 10.9, 'Very flexible', 40);
INSERT INTO Varinha VALUES ( 55, 'Blackthorn', 'Troll whisker', 7.1, 'Very flexible', 104);
INSERT INTO Varinha VALUES ( 56, 'Pine', 'Unicorn hair', 6.04, 'Very flexible', 137);
INSERT INTO Varinha VALUES ( 57, 'Willow', 'Veela hair', 9.96, 'Very flexible', 134);
INSERT INTO Varinha VALUES ( 58, 'Apple', 'Veela hair', 9.12, 'Little Flexivel', 4);
INSERT INTO Varinha VALUES ( 59, 'Cypress', 'Troll whisker', 10.69, 'Little Flexivel', 82);
INSERT INTO Varinha VALUES ( 60, 'Ash', 'Basilisk horn', 12.56, 'Very flexible', 62);
INSERT INTO Varinha VALUES ( 61, 'Apple', 'Dragon heartstring', 11.79, 'Little Flexivel', 47);
INSERT INTO Varinha VALUES ( 62, 'Maple', 'Thestral tail hair', 7.61, 'Very flexible', 48);
INSERT INTO Varinha VALUES ( 63, 'Acacia', 'Phoenix feather', 9.84, 'Very flexible', 41);
INSERT INTO Varinha VALUES ( 64, 'Cypress', 'Dragon heartstring', 10.32, 'Little Flexivel', 80);
INSERT INTO Varinha VALUES ( 65, 'Willow', 'Phoenix feather', 12.32, 'Very flexible', 23);
INSERT INTO Varinha VALUES ( 66, 'Cherry', 'Thestral tail hair', 7.92, 'Very flexible', 135);
INSERT INTO Varinha VALUES ( 67, 'Hazel', 'Troll whisker', 7.4, 'Little Flexivel', 137);
INSERT INTO Varinha VALUES ( 68, 'Alder', 'Troll whisker', 8.61, 'Very flexible', 139);
INSERT INTO Varinha VALUES ( 69, 'Pine', 'Veela hair', 9.15, 'Very flexible', 123);
INSERT INTO Varinha VALUES ( 70, 'Ebony', 'Thestral tail hair', 10.93, 'Very flexible', 110);
INSERT INTO Varinha VALUES ( 71, 'Cherry', 'Phoenix feather', 7.74, 'Little Flexivel', 128);
INSERT INTO Varinha VALUES ( 72, 'Alder', 'Unicorn hair', 11.2, 'Little Flexivel', 130);
INSERT INTO Varinha VALUES ( 73, 'Willow', 'Unicorn hair', 11.19, 'Little Flexivel', 138);
INSERT INTO Varinha VALUES ( 74, 'Vine', 'Veela hair', 6.59, 'Very flexible', 114);
INSERT INTO Varinha VALUES ( 75, 'Cherry', 'Dragon heartstring', 11.58, 'Little Flexivel', 132);
INSERT INTO Varinha VALUES ( 76, 'Ebony', 'Kelpie hair', 11.27, 'Little Flexivel', 68);
INSERT INTO Varinha VALUES ( 77, 'Cypress', 'Troll whisker', 11.93, 'Very flexible', 97);
INSERT INTO Varinha VALUES ( 78, 'Alder', 'Unicorn hair', 9.85, 'Little Flexivel', 56);
INSERT INTO Varinha VALUES ( 79, 'Apple', 'Thestral tail hair', 12.89, 'Very flexible', 136);
INSERT INTO Varinha VALUES ( 80, 'Apple', 'Kelpie hair', 8.45, 'Little Flexivel', 125);
INSERT INTO Varinha VALUES ( 81, 'Ebony', 'Dragon heartstring', 10.31, 'Little Flexivel', 23);
INSERT INTO Varinha VALUES ( 82, 'Cherry', 'Unicorn hair', 8.53, 'Very flexible', 50);
INSERT INTO Varinha VALUES ( 83, 'Silver lime', 'Troll whisker', 8.45, 'Very flexible', 40);
INSERT INTO Varinha VALUES ( 84, 'Ash', 'Dragon heartstring', 9.64, 'Very flexible', 83);
INSERT INTO Varinha VALUES ( 85, 'Willow', 'Veela hair', 7.48, 'Very flexible', 140);
INSERT INTO Varinha VALUES ( 86, 'Hazel', 'Phoenix feather', 9.87, 'Little Flexivel', 5);
INSERT INTO Varinha VALUES ( 87, 'Ebony', 'Veela hair', 7.2, 'Very flexible', 11);
INSERT INTO Varinha VALUES ( 88, 'Silver lime', 'Veela hair', 11.0, 'Little Flexivel', 93);
INSERT INTO Varinha VALUES ( 89, 'Pine', 'Dragon heartstring', 9.0, 'Very flexible', 110);
INSERT INTO Varinha VALUES ( 90, 'Maple', 'Troll whisker', 8.43, 'Little Flexivel', 52);
INSERT INTO Varinha VALUES ( 91, 'Acacia', 'Kelpie hair', 8.44, 'Very flexible', 8);
INSERT INTO Varinha VALUES ( 92, 'Ash', 'Kelpie hair', 12.51, 'Very flexible', 83);
INSERT INTO Varinha VALUES ( 93, 'Acacia', 'Basilisk horn', 12.26, 'Little Flexivel', 116);
INSERT INTO Varinha VALUES ( 94, 'Vine', 'Troll whisker', 7.55, 'Little Flexivel', 52);
INSERT INTO Varinha VALUES ( 95, 'Maple', 'Phoenix feather', 7.34, 'Little Flexivel', 39);
INSERT INTO Varinha VALUES ( 96, 'Vine', 'Kelpie hair', 11.68, 'Little Flexivel', 48);
INSERT INTO Varinha VALUES ( 97, 'Vine', 'Kelpie hair', 8.55, 'Very flexible', 36);
INSERT INTO Varinha VALUES ( 98, 'Cherry', 'Unicorn hair', 11.18, 'Little Flexivel', 108);
INSERT INTO Varinha VALUES ( 99, 'Ash', 'Kelpie hair', 10.33, 'Very flexible', 51);
INSERT INTO Varinha VALUES ( 100, 'Vine', 'Troll whisker', 9.56, 'Very flexible', 2);
INSERT INTO Varinha VALUES ( 101, 'Maple', 'Basilisk horn', 10.18, 'Very flexible', 81);
INSERT INTO Varinha VALUES ( 102, 'Silver lime', 'Thestral tail hair', 9.67, 'Very flexible', 98);
INSERT INTO Varinha VALUES ( 103, 'Willow', 'Unicorn hair', 6.84, 'Little Flexivel', 137);
INSERT INTO Varinha VALUES ( 104, 'Cypress', 'Kelpie hair', 8.87, 'Little Flexivel', 103);
INSERT INTO Varinha VALUES ( 105, 'Ash', 'Thestral tail hair', 11.57, 'Little Flexivel', 80);
INSERT INTO Varinha VALUES ( 106, 'Silver lime', 'Unicorn hair', 12.78, 'Very flexible', 51);
INSERT INTO Varinha VALUES ( 107, 'Willow', 'Thestral tail hair', 6.95, 'Little Flexivel', 119);
INSERT INTO Varinha VALUES ( 108, 'Hazel', 'Veela hair', 7.73, 'Little Flexivel', 25);
INSERT INTO Varinha VALUES ( 109, 'Silver lime', 'Basilisk horn', 7.39, 'Very flexible', 6);
INSERT INTO Varinha VALUES ( 110, 'Willow', 'Troll whisker', 11.13, 'Little Flexivel', 71);
INSERT INTO Varinha VALUES ( 111, 'Apple', 'Unicorn hair', 11.21, 'Very flexible', 98);
INSERT INTO Varinha VALUES ( 112, 'Blackthorn', 'Thestral tail hair', 12.48, 'Little Flexivel', 81);
INSERT INTO Varinha VALUES ( 113, 'Hazel', 'Thestral tail hair', 12.47, 'Very flexible', 43);
INSERT INTO Varinha VALUES ( 114, 'Blackthorn', 'Troll whisker', 8.19, 'Very flexible', 35);
INSERT INTO Varinha VALUES ( 115, 'Silver lime', 'Thestral tail hair', 12.42, 'Very flexible', 21);
INSERT INTO Varinha VALUES ( 116, 'Acacia', 'Veela hair', 9.24, 'Very flexible', 5);
INSERT INTO Varinha VALUES ( 117, 'Hazel', 'Dragon heartstring', 7.82, 'Very flexible', 46);
INSERT INTO Varinha VALUES ( 118, 'Hazel', 'Basilisk horn', 7.16, 'Little Flexivel', 96);
INSERT INTO Varinha VALUES ( 119, 'Cherry', 'Phoenix feather', 7.15, 'Very flexible', 33);
INSERT INTO Varinha VALUES ( 120, 'Pine', 'Troll whisker', 6.93, 'Little Flexivel', 129);
INSERT INTO Varinha VALUES ( 121, 'Ebony', 'Troll whisker', 6.32, 'Very flexible', 103);
INSERT INTO Varinha VALUES ( 122, 'Maple', 'Troll whisker', 10.9, 'Very flexible', 1);
INSERT INTO Varinha VALUES ( 123, 'Cherry', 'Kelpie hair', 12.67, 'Very flexible', 86);
INSERT INTO Varinha VALUES ( 124, 'Ebony', 'Kelpie hair', 10.56, 'Very flexible', 90);
INSERT INTO Varinha VALUES ( 125, 'Silver lime', 'Kelpie hair', 7.19, 'Little Flexivel', 102);
INSERT INTO Varinha VALUES ( 126, 'Silver lime', 'Kelpie hair', 7.01, 'Very flexible', 128);
INSERT INTO Varinha VALUES ( 127, 'Maple', 'Kelpie hair', 6.18, 'Very flexible', 45);
INSERT INTO Varinha VALUES ( 128, 'Willow', 'Dragon heartstring', 8.21, 'Little Flexivel', 49);
INSERT INTO Varinha VALUES ( 129, 'Silver lime', 'Dragon heartstring', 7.42, 'Very flexible', 20);
INSERT INTO Varinha VALUES ( 130, 'Ebony', 'Thestral tail hair', 9.23, 'Little Flexivel', 64);
INSERT INTO Varinha VALUES ( 131, 'Acacia', 'Unicorn hair', 7.31, 'Little Flexivel', 20);
INSERT INTO Varinha VALUES ( 132, 'Ash', 'Dragon heartstring', 10.48, 'Very flexible', 138);
INSERT INTO Varinha VALUES ( 133, 'Cherry', 'Thestral tail hair', 9.03, 'Very flexible', 40);
INSERT INTO Varinha VALUES ( 134, 'Hazel', 'Dragon heartstring', 8.27, 'Very flexible', 46);
INSERT INTO Varinha VALUES ( 135, 'Pine', 'Dragon heartstring', 9.73, 'Very flexible', 111);
INSERT INTO Varinha VALUES ( 136, 'Cypress', 'Troll whisker', 10.06, 'Very flexible', 99);
INSERT INTO Varinha VALUES ( 137, 'Hazel', 'Veela hair', 11.77, 'Little Flexivel', 49);
INSERT INTO Varinha VALUES ( 138, 'Maple', 'Thestral tail hair', 10.34, 'Little Flexivel', 107);
INSERT INTO Varinha VALUES ( 139, 'Vine', 'Kelpie hair', 9.93, 'Little Flexivel', 21);
INSERT INTO Varinha VALUES ( 140, 'Pine', 'Troll whisker', 6.22, 'Very flexible', 82);
INSERT INTO Varinha VALUES ( 141, 'Blackthorn', 'Kelpie hair', 12.83, 'Little Flexivel', 136);
INSERT INTO Varinha VALUES ( 142, 'Apple', 'Unicorn hair', 10.97, 'Little Flexivel', 13);
INSERT INTO Varinha VALUES ( 143, 'Cherry', 'Basilisk horn', 8.56, 'Very flexible', 7);
INSERT INTO Varinha VALUES ( 144, 'Hazel', 'Troll whisker', 11.77, 'Very flexible', 28);
INSERT INTO Varinha VALUES ( 145, 'Apple', 'Unicorn hair', 10.79, 'Little Flexivel', 75);
INSERT INTO Varinha VALUES ( 146, 'Cherry', 'Troll whisker', 8.88, 'Very flexible', 37);
INSERT INTO Varinha VALUES ( 147, 'Apple', 'Troll whisker', 9.29, 'Very flexible', 106);
INSERT INTO Varinha VALUES ( 148, 'Ash', 'Thestral tail hair', 10.23, 'Little Flexivel', 51);
INSERT INTO Varinha VALUES ( 149, 'Pine', 'Thestral tail hair', 8.91, 'Very flexible', 25);
INSERT INTO Varinha VALUES ( 150, 'Maple', 'Kelpie hair', 8.06, 'Little Flexivel', 45);
INSERT INTO Varinha VALUES ( 151, 'Ash', 'Dragon heartstring', 8.45, 'Little Flexivel', 114);
INSERT INTO Varinha VALUES ( 152, 'Vine', 'Veela hair', 11.59, 'Very flexible', 24);
INSERT INTO Varinha VALUES ( 153, 'Apple', 'Phoenix feather', 7.24, 'Very flexible', 129);
INSERT INTO Varinha VALUES ( 154, 'Willow', 'Veela hair', 11.17, 'Little Flexivel', 35);
INSERT INTO Varinha VALUES ( 155, 'Hazel', 'Basilisk horn', 9.58, 'Very flexible', 40);
INSERT INTO Varinha VALUES ( 156, 'Blackthorn', 'Thestral tail hair', 6.52, 'Little Flexivel', 79);
INSERT INTO Varinha VALUES ( 157, 'Alder', 'Thestral tail hair', 8.2, 'Very flexible', 123);
INSERT INTO Varinha VALUES ( 158, 'Vine', 'Basilisk horn', 12.11, 'Very flexible', 103);
INSERT INTO Varinha VALUES ( 159, 'Acacia', 'Unicorn hair', 7.67, 'Little Flexivel', 77);
INSERT INTO Varinha VALUES ( 160, 'Cherry', 'Veela hair', 9.62, 'Very flexible', 64);
INSERT INTO Varinha VALUES ( 161, 'Ash', 'Dragon heartstring', 11.43, 'Little Flexivel', 90);
INSERT INTO Varinha VALUES ( 162, 'Blackthorn', 'Thestral tail hair', 6.79, 'Very flexible', 69);
INSERT INTO Varinha VALUES ( 163, 'Cypress', 'Kelpie hair', 10.04, 'Little Flexivel', 27);
INSERT INTO Varinha VALUES ( 164, 'Hazel', 'Dragon heartstring', 10.85, 'Little Flexivel', 116);
INSERT INTO Varinha VALUES ( 165, 'Acacia', 'Troll whisker', 10.69, 'Very flexible', 54);
INSERT INTO Varinha VALUES ( 166, 'Willow', 'Kelpie hair', 7.16, 'Very flexible', 92);
INSERT INTO Varinha VALUES ( 167, 'Ebony', 'Basilisk horn', 11.55, 'Very flexible', 2);
INSERT INTO Varinha VALUES ( 168, 'Ash', 'Veela hair', 10.06, 'Very flexible', 24);
INSERT INTO Varinha VALUES ( 169, 'Ash', 'Phoenix feather', 10.91, 'Very flexible', 51);
INSERT INTO Varinha VALUES ( 170, 'Alder', 'Dragon heartstring', 9.24, 'Little Flexivel', 45);
INSERT INTO Varinha VALUES ( 171, 'Alder', 'Phoenix feather', 9.12, 'Very flexible', 119);
INSERT INTO Varinha VALUES ( 172, 'Maple', 'Kelpie hair', 9.79, 'Little Flexivel', 111);
INSERT INTO Varinha VALUES ( 173, 'Apple', 'Kelpie hair', 7.88, 'Very flexible', 26);
INSERT INTO Varinha VALUES ( 174, 'Blackthorn', 'Unicorn hair', 12.68, 'Little Flexivel', 15);
INSERT INTO Varinha VALUES ( 175, 'Silver lime', 'Phoenix feather', 8.72, 'Very flexible', 76);
INSERT INTO Varinha VALUES ( 176, 'Hazel', 'Unicorn hair', 8.53, 'Little Flexivel', 116);
INSERT INTO Varinha VALUES ( 177, 'Ebony', 'Unicorn hair', 7.97, 'Little Flexivel', 132);
INSERT INTO Varinha VALUES ( 178, 'Vine', 'Dragon heartstring', 8.27, 'Little Flexivel', 18);
INSERT INTO Varinha VALUES ( 179, 'Cherry', 'Thestral tail hair', 7.11, 'Very flexible', 90);
INSERT INTO Varinha VALUES ( 180, 'Pine', 'Basilisk horn', 7.31, 'Very flexible', 87);
INSERT INTO Varinha VALUES ( 181, 'Ebony', 'Thestral tail hair', 8.34, 'Very flexible', 18);
INSERT INTO Varinha VALUES ( 182, 'Pine', 'Thestral tail hair', 10.6, 'Little Flexivel', 25);
INSERT INTO Varinha VALUES ( 183, 'Cypress', 'Thestral tail hair', 11.8, 'Little Flexivel', 74);
INSERT INTO Varinha VALUES ( 184, 'Acacia', 'Unicorn hair', 7.19, 'Very flexible', 58);
INSERT INTO Varinha VALUES ( 185, 'Hazel', 'Thestral tail hair', 7.99, 'Little Flexivel', 136);
INSERT INTO Varinha VALUES ( 186, 'Acacia', 'Thestral tail hair', 6.78, 'Little Flexivel', 80);
INSERT INTO Varinha VALUES ( 187, 'Ash', 'Thestral tail hair', 6.61, 'Very flexible', 120);
INSERT INTO Varinha VALUES ( 188, 'Hazel', 'Dragon heartstring', 8.87, 'Little Flexivel', 140);
INSERT INTO Varinha VALUES ( 189, 'Cherry', 'Dragon heartstring', 8.01, 'Very flexible', 98);
INSERT INTO Varinha VALUES ( 190, 'Maple', 'Troll whisker', 8.83, 'Very flexible', 99);
INSERT INTO Varinha VALUES ( 191, 'Apple', 'Troll whisker', 7.68, 'Very flexible', 89);
INSERT INTO Varinha VALUES ( 192, 'Pine', 'Kelpie hair', 7.6, 'Little Flexivel', 133);
INSERT INTO Varinha VALUES ( 193, 'Willow', 'Unicorn hair', 10.47, 'Very flexible', 102);
INSERT INTO Varinha VALUES ( 194, 'Ebony', 'Dragon heartstring', 6.01, 'Little Flexivel', 131);
INSERT INTO Varinha VALUES ( 195, 'Cypress', 'Unicorn hair', 12.78, 'Very flexible', 66);
INSERT INTO Varinha VALUES ( 196, 'Cypress', 'Unicorn hair', 8.46, 'Little Flexivel', 84);
INSERT INTO Varinha VALUES ( 197, 'Apple', 'Phoenix feather', 12.94, 'Very flexible', 80);
INSERT INTO Varinha VALUES ( 198, 'Pine', 'Phoenix feather', 10.63, 'Very flexible', 82);
INSERT INTO Varinha VALUES ( 199, 'Ebony', 'Unicorn hair', 12.44, 'Little Flexivel', 57);

/* Como os dados de dataAprendeu para a tabela Saber foram gerados aleatoriamente, 
essa trigger verifica se há casos em que dataAprendeu é uma data anterior à data
de nascimento do personagem. Nesse caso, dataAprendeu é setada para a data de
nascimento acrescida de 11 anos*/
CREATE OR REPLACE FUNCTION dataValida ()
RETURNS TRIGGER AS $validar$
DECLARE
	nasc date;
	aprendeu date := NEW.dataAprendeu;
BEGIN
	SELECT dataNasc INTO nasc
	FROM Personagem
	WHERE personagemPK = NEW.personagemFK;

	IF extract(year from nasc) + 11 > extract(year from aprendeu) THEN
		NEW.dataAprendeu := nasc + interval '11 YEARS';
	END IF;
	RETURN NEW;
END;
$validar$ LANGUAGE plpgsql;
CREATE TRIGGER validar
BEFORE INSERT ON Saber
FOR EACH ROW EXECUTE PROCEDURE dataValida();

INSERT INTO Saber VALUES ( 96, 39, '6-1-1972');
INSERT INTO Saber VALUES ( 13, 9, '4-1-1991');
INSERT INTO Saber VALUES ( 17, 73, '18-1-1983');
INSERT INTO Saber VALUES ( 216, 140, '28-9-1991');
INSERT INTO Saber VALUES ( 280, 69, '10-8-1993');
INSERT INTO Saber VALUES ( 203, 44, '4-5-1987');
INSERT INTO Saber VALUES ( 102, 55, '20-8-1961');
INSERT INTO Saber VALUES ( 140, 120, '1-4-1971');
INSERT INTO Saber VALUES ( 289, 107, '19-12-2019');
INSERT INTO Saber VALUES ( 204, 132, '12-9-2004');
INSERT INTO Saber VALUES ( 145, 119, '27-8-1966');
INSERT INTO Saber VALUES ( 239, 46, '19-10-2019');
INSERT INTO Saber VALUES ( 194, 131, '27-6-1970');
INSERT INTO Saber VALUES ( 1, 12, '20-8-2011');
INSERT INTO Saber VALUES ( 11, 73, '17-11-1997');
INSERT INTO Saber VALUES ( 147, 96, '23-12-2018');
INSERT INTO Saber VALUES ( 185, 124, '4-9-1987');
INSERT INTO Saber VALUES ( 50, 94, '1-1-1993');
INSERT INTO Saber VALUES ( 86, 24, '14-6-1997');
INSERT INTO Saber VALUES ( 67, 94, '4-5-2016');
INSERT INTO Saber VALUES ( 195, 34, '20-9-2006');
INSERT INTO Saber VALUES ( 269, 115, '13-8-1990');
INSERT INTO Saber VALUES ( 196, 109, '17-2-2018');
INSERT INTO Saber VALUES ( 241, 134, '14-10-1963');
INSERT INTO Saber VALUES ( 263, 46, '15-8-1973');
INSERT INTO Saber VALUES ( 234, 60, '1-8-1963');
INSERT INTO Saber VALUES ( 183, 22, '23-4-1981');
INSERT INTO Saber VALUES ( 45, 140, '17-11-1971');
INSERT INTO Saber VALUES ( 220, 126, '18-11-1989');
INSERT INTO Saber VALUES ( 199, 67, '13-3-1971');
INSERT INTO Saber VALUES ( 286, 129, '21-8-2004');
INSERT INTO Saber VALUES ( 56, 133, '3-6-1966');
INSERT INTO Saber VALUES ( 202, 39, '1-8-1960');
INSERT INTO Saber VALUES ( 229, 73, '28-7-2014');
INSERT INTO Saber VALUES ( 186, 99, '16-11-1972');
INSERT INTO Saber VALUES ( 135, 66, '16-3-1966');
INSERT INTO Saber VALUES ( 243, 39, '25-4-1997');
INSERT INTO Saber VALUES ( 165, 104, '4-6-2011');
INSERT INTO Saber VALUES ( 43, 133, '18-4-2014');
INSERT INTO Saber VALUES ( 108, 55, '7-7-1967');
INSERT INTO Saber VALUES ( 154, 26, '15-10-1968');
INSERT INTO Saber VALUES ( 246, 11, '26-7-1981');
INSERT INTO Saber VALUES ( 170, 68, '17-9-1979');
INSERT INTO Saber VALUES ( 181, 65, '23-6-1976');
INSERT INTO Saber VALUES ( 40, 103, '28-8-2010');
INSERT INTO Saber VALUES ( 147, 23, '11-1-2011');
INSERT INTO Saber VALUES ( 137, 131, '20-2-1979');
INSERT INTO Saber VALUES ( 75, 120, '23-3-2008');
INSERT INTO Saber VALUES ( 3, 58, '16-12-2017');
INSERT INTO Saber VALUES ( 206, 58, '21-12-1983');
INSERT INTO Saber VALUES ( 117, 70, '9-11-1971');
INSERT INTO Saber VALUES ( 73, 134, '8-1-1978');
INSERT INTO Saber VALUES ( 142, 70, '23-7-1960');
INSERT INTO Saber VALUES ( 120, 102, '17-1-1988');
INSERT INTO Saber VALUES ( 153, 120, '10-5-1965');
INSERT INTO Saber VALUES ( 259, 41, '28-12-2013');
INSERT INTO Saber VALUES ( 293, 74, '1-6-2012');
INSERT INTO Saber VALUES ( 127, 117, '4-8-1992');
INSERT INTO Saber VALUES ( 197, 61, '10-1-1980');
INSERT INTO Saber VALUES ( 192, 116, '9-12-1972');
INSERT INTO Saber VALUES ( 63, 136, '19-12-2004');
INSERT INTO Saber VALUES ( 8, 120, '24-6-2000');
INSERT INTO Saber VALUES ( 205, 22, '6-8-1991');
INSERT INTO Saber VALUES ( 233, 16, '4-12-2014');
INSERT INTO Saber VALUES ( 9, 2, '12-1-1979');
INSERT INTO Saber VALUES ( 185, 108, '22-6-1982');
INSERT INTO Saber VALUES ( 76, 19, '24-3-1964');
INSERT INTO Saber VALUES ( 148, 124, '25-4-1985');
INSERT INTO Saber VALUES ( 270, 93, '12-10-1984');
INSERT INTO Saber VALUES ( 2, 50, '3-4-1979');
INSERT INTO Saber VALUES ( 291, 117, '21-5-1990');
INSERT INTO Saber VALUES ( 60, 125, '6-8-1975');
INSERT INTO Saber VALUES ( 254, 101, '22-12-1972');
INSERT INTO Saber VALUES ( 163, 66, '27-12-1970');
INSERT INTO Saber VALUES ( 96, 16, '13-5-1967');
INSERT INTO Saber VALUES ( 122, 35, '8-1-1999');
INSERT INTO Saber VALUES ( 216, 43, '28-8-1969');
INSERT INTO Saber VALUES ( 274, 46, '5-1-2011');
INSERT INTO Saber VALUES ( 42, 75, '23-9-2019');
INSERT INTO Saber VALUES ( 47, 15, '11-7-2015');
INSERT INTO Saber VALUES ( 81, 89, '27-1-2010');
INSERT INTO Saber VALUES ( 116, 58, '17-2-1994');
INSERT INTO Saber VALUES ( 284, 41, '15-8-1973');
INSERT INTO Saber VALUES ( 213, 83, '7-9-1995');
INSERT INTO Saber VALUES ( 33, 98, '24-6-1962');
INSERT INTO Saber VALUES ( 292, 39, '16-8-1986');
INSERT INTO Saber VALUES ( 36, 34, '21-4-1995');
INSERT INTO Saber VALUES ( 116, 4, '1-2-2001');
INSERT INTO Saber VALUES ( 157, 43, '7-11-1982');
INSERT INTO Saber VALUES ( 38, 19, '20-6-2006');
INSERT INTO Saber VALUES ( 258, 29, '2-11-1987');
INSERT INTO Saber VALUES ( 51, 37, '19-3-2014');
INSERT INTO Saber VALUES ( 117, 110, '12-4-1994');
INSERT INTO Saber VALUES ( 252, 21, '6-12-2016');
INSERT INTO Saber VALUES ( 36, 137, '20-1-2013');
INSERT INTO Saber VALUES ( 132, 98, '22-1-2008');
INSERT INTO Saber VALUES ( 113, 130, '15-11-2007');
INSERT INTO Saber VALUES ( 144, 97, '2-10-2020');
INSERT INTO Saber VALUES ( 259, 1, '6-4-1962');
INSERT INTO Saber VALUES ( 12, 48, '4-6-2012');
INSERT INTO Saber VALUES ( 260, 1, '19-6-2007');
INSERT INTO Saber VALUES ( 61, 125, '19-11-2016');
INSERT INTO Saber VALUES ( 75, 14, '1-4-1986');
INSERT INTO Saber VALUES ( 183, 52, '18-3-2014');
INSERT INTO Saber VALUES ( 106, 9, '15-2-2001');
INSERT INTO Saber VALUES ( 112, 98, '8-12-2012');
INSERT INTO Saber VALUES ( 214, 73, '22-3-1982');
INSERT INTO Saber VALUES ( 10, 15, '25-4-1980');
INSERT INTO Saber VALUES ( 139, 42, '16-5-1992');
INSERT INTO Saber VALUES ( 251, 56, '19-4-1977');
INSERT INTO Saber VALUES ( 296, 101, '5-4-1972');
INSERT INTO Saber VALUES ( 47, 94, '14-11-1985');
INSERT INTO Saber VALUES ( 108, 40, '2-7-1962');
INSERT INTO Saber VALUES ( 139, 77, '1-2-1969');
INSERT INTO Saber VALUES ( 96, 126, '2-1-1987');
INSERT INTO Saber VALUES ( 3, 98, '12-10-2019');
INSERT INTO Saber VALUES ( 32, 115, '9-2-2008');
INSERT INTO Saber VALUES ( 285, 77, '28-8-1990');
INSERT INTO Saber VALUES ( 112, 121, '21-3-1964');
INSERT INTO Saber VALUES ( 293, 140, '22-1-2011');
INSERT INTO Saber VALUES ( 143, 105, '23-6-1963');
INSERT INTO Saber VALUES ( 102, 119, '7-7-1969');
INSERT INTO Saber VALUES ( 274, 43, '18-6-2016');
INSERT INTO Saber VALUES ( 208, 139, '22-8-1962');
INSERT INTO Saber VALUES ( 12, 38, '4-7-2000');
INSERT INTO Saber VALUES ( 1, 16, '6-1-1984');
INSERT INTO Saber VALUES ( 86, 101, '5-7-2017');
INSERT INTO Saber VALUES ( 271, 136, '27-7-1983');
INSERT INTO Saber VALUES ( 273, 76, '25-7-2016');
INSERT INTO Saber VALUES ( 259, 120, '17-2-1989');
INSERT INTO Saber VALUES ( 159, 27, '25-5-2008');
INSERT INTO Saber VALUES ( 256, 108, '9-11-2009');
INSERT INTO Saber VALUES ( 47, 81, '24-4-1963');
INSERT INTO Saber VALUES ( 131, 114, '23-4-2017');
INSERT INTO Saber VALUES ( 209, 135, '22-4-2013');
INSERT INTO Saber VALUES ( 175, 130, '4-2-2017');
INSERT INTO Saber VALUES ( 142, 26, '13-4-1999');
INSERT INTO Saber VALUES ( 256, 83, '6-11-2001');
INSERT INTO Saber VALUES ( 209, 46, '1-10-1980');
INSERT INTO Saber VALUES ( 15, 139, '26-10-2003');
INSERT INTO Saber VALUES ( 127, 16, '24-7-1961');
INSERT INTO Saber VALUES ( 293, 107, '8-3-1972');
INSERT INTO Saber VALUES ( 293, 85, '8-1-2013');
INSERT INTO Saber VALUES ( 164, 84, '19-10-1995');
INSERT INTO Saber VALUES ( 17, 77, '9-6-1984');
INSERT INTO Saber VALUES ( 200, 108, '5-8-1989');
INSERT INTO Saber VALUES ( 221, 67, '11-5-2008');
INSERT INTO Saber VALUES ( 287, 62, '15-12-2006');
INSERT INTO Saber VALUES ( 256, 98, '10-6-1996');
INSERT INTO Saber VALUES ( 180, 104, '5-10-1982');
INSERT INTO Saber VALUES ( 275, 121, '7-8-1981');
INSERT INTO Saber VALUES ( 231, 88, '9-3-2017');
INSERT INTO Saber VALUES ( 256, 135, '2-1-2005');
INSERT INTO Saber VALUES ( 82, 60, '5-10-1989');
INSERT INTO Saber VALUES ( 233, 115, '9-2-1998');
INSERT INTO Saber VALUES ( 162, 15, '7-2-1969');
INSERT INTO Saber VALUES ( 265, 41, '23-8-1968');
INSERT INTO Saber VALUES ( 166, 140, '6-11-1962');
INSERT INTO Saber VALUES ( 239, 72, '7-8-1970');
INSERT INTO Saber VALUES ( 276, 133, '10-8-1977');
INSERT INTO Saber VALUES ( 215, 6, '26-11-2018');
INSERT INTO Saber VALUES ( 275, 81, '22-1-1978');
INSERT INTO Saber VALUES ( 217, 129, '13-9-1980');
INSERT INTO Saber VALUES ( 26, 120, '14-11-1997');
INSERT INTO Saber VALUES ( 20, 76, '21-6-1989');
INSERT INTO Saber VALUES ( 151, 3, '2-10-1969');
INSERT INTO Saber VALUES ( 285, 96, '27-7-2007');
INSERT INTO Saber VALUES ( 255, 115, '4-4-2007');
INSERT INTO Saber VALUES ( 8, 14, '4-8-1962');
INSERT INTO Saber VALUES ( 99, 34, '25-7-1990');
INSERT INTO Saber VALUES ( 14, 59, '7-8-1985');
INSERT INTO Saber VALUES ( 288, 64, '5-5-2002');
INSERT INTO Saber VALUES ( 68, 72, '8-10-1991');
INSERT INTO Saber VALUES ( 175, 15, '20-9-1975');
INSERT INTO Saber VALUES ( 289, 21, '7-5-2004');
INSERT INTO Saber VALUES ( 231, 115, '26-8-2018');
INSERT INTO Saber VALUES ( 234, 139, '18-5-1982');
INSERT INTO Saber VALUES ( 268, 52, '24-12-2002');
INSERT INTO Saber VALUES ( 148, 33, '14-4-2000');
INSERT INTO Saber VALUES ( 199, 51, '5-11-1994');
INSERT INTO Saber VALUES ( 107, 119, '1-6-2006');
INSERT INTO Saber VALUES ( 194, 94, '18-8-1982');
INSERT INTO Saber VALUES ( 72, 78, '6-6-1990');
INSERT INTO Saber VALUES ( 251, 51, '7-9-2005');
INSERT INTO Saber VALUES ( 285, 26, '23-8-2003');
INSERT INTO Saber VALUES ( 59, 52, '16-11-2017');
INSERT INTO Saber VALUES ( 295, 38, '20-12-1988');
INSERT INTO Saber VALUES ( 80, 40, '14-2-1976');
INSERT INTO Saber VALUES ( 76, 66, '18-3-1999');
INSERT INTO Saber VALUES ( 215, 106, '1-3-1991');
INSERT INTO Saber VALUES ( 207, 56, '25-5-1974');
INSERT INTO Saber VALUES ( 59, 65, '11-7-2012');
INSERT INTO Saber VALUES ( 233, 47, '13-3-1990');
INSERT INTO Saber VALUES ( 276, 86, '5-7-1997');
INSERT INTO Saber VALUES ( 207, 110, '27-5-1966');
INSERT INTO Saber VALUES ( 5, 120, '21-7-1983');
INSERT INTO Saber VALUES ( 174, 23, '5-4-1981');
INSERT INTO Saber VALUES ( 141, 46, '13-2-1989');
INSERT INTO Saber VALUES ( 247, 114, '9-3-2001');
INSERT INTO Saber VALUES ( 291, 46, '24-6-2000');
INSERT INTO Saber VALUES ( 15, 113, '16-11-2004');
INSERT INTO Saber VALUES ( 187, 20, '14-5-1973');
INSERT INTO Saber VALUES ( 100, 120, '26-3-1987');
INSERT INTO Saber VALUES ( 194, 16, '9-9-2015');
INSERT INTO Saber VALUES ( 93, 98, '2-5-1976');
INSERT INTO Saber VALUES ( 278, 103, '16-6-2017');
INSERT INTO Saber VALUES ( 25, 136, '5-11-1974');
INSERT INTO Saber VALUES ( 231, 132, '1-9-2011');
INSERT INTO Saber VALUES ( 109, 138, '16-2-1993');
INSERT INTO Saber VALUES ( 26, 15, '9-11-2000');
INSERT INTO Saber VALUES ( 64, 91, '3-2-1979');
INSERT INTO Saber VALUES ( 49, 26, '12-12-1995');
INSERT INTO Saber VALUES ( 251, 109, '18-8-1960');
INSERT INTO Saber VALUES ( 203, 119, '13-11-1972');
INSERT INTO Saber VALUES ( 124, 126, '10-3-1966');
INSERT INTO Saber VALUES ( 283, 136, '27-1-1997');
INSERT INTO Saber VALUES ( 228, 26, '14-1-2014');
INSERT INTO Saber VALUES ( 141, 65, '9-2-1978');
INSERT INTO Saber VALUES ( 46, 4, '18-4-1982');
INSERT INTO Saber VALUES ( 188, 103, '18-12-1987');
INSERT INTO Saber VALUES ( 72, 114, '6-12-2006');
INSERT INTO Saber VALUES ( 207, 136, '28-1-1995');
INSERT INTO Saber VALUES ( 243, 32, '20-1-1962');
INSERT INTO Saber VALUES ( 67, 19, '16-10-1974');
INSERT INTO Saber VALUES ( 114, 25, '3-5-1988');
INSERT INTO Saber VALUES ( 84, 90, '22-8-2002');
INSERT INTO Saber VALUES ( 101, 69, '5-8-2008');
INSERT INTO Saber VALUES ( 127, 33, '10-2-1961');
INSERT INTO Saber VALUES ( 218, 24, '15-9-2009');
INSERT INTO Saber VALUES ( 184, 39, '28-2-2019');
INSERT INTO Saber VALUES ( 27, 90, '10-6-2009');
INSERT INTO Saber VALUES ( 64, 90, '23-10-2013');
INSERT INTO Saber VALUES ( 13, 5, '8-10-1979');
INSERT INTO Saber VALUES ( 220, 93, '16-12-1980');
INSERT INTO Saber VALUES ( 265, 97, '7-3-2000');
INSERT INTO Saber VALUES ( 106, 110, '25-11-2002');
INSERT INTO Saber VALUES ( 242, 13, '19-10-2004');
INSERT INTO Saber VALUES ( 205, 41, '22-5-1982');
INSERT INTO Saber VALUES ( 98, 44, '14-7-2011');
INSERT INTO Saber VALUES ( 39, 87, '15-10-2003');
INSERT INTO Saber VALUES ( 126, 3, '16-9-1969');
INSERT INTO Saber VALUES ( 298, 84, '17-12-1975');
INSERT INTO Saber VALUES ( 227, 42, '23-12-1968');
INSERT INTO Saber VALUES ( 152, 111, '4-3-2008');
INSERT INTO Saber VALUES ( 140, 116, '3-8-1993');
INSERT INTO Saber VALUES ( 73, 53, '16-5-2003');
INSERT INTO Saber VALUES ( 182, 110, '27-9-1985');
INSERT INTO Saber VALUES ( 32, 110, '6-4-1962');
INSERT INTO Saber VALUES ( 151, 75, '1-5-1991');
INSERT INTO Saber VALUES ( 36, 102, '14-11-1987');
INSERT INTO Saber VALUES ( 13, 39, '9-8-1983');
INSERT INTO Saber VALUES ( 285, 37, '2-10-2006');
INSERT INTO Saber VALUES ( 104, 91, '21-12-1974');
INSERT INTO Saber VALUES ( 298, 88, '3-3-1987');
INSERT INTO Saber VALUES ( 159, 80, '6-1-1995');
INSERT INTO Saber VALUES ( 101, 119, '21-4-1983');
INSERT INTO Saber VALUES ( 125, 97, '10-1-1971');
INSERT INTO Saber VALUES ( 173, 81, '11-8-2000');
INSERT INTO Saber VALUES ( 194, 132, '24-12-2013');
INSERT INTO Saber VALUES ( 24, 45, '20-2-1968');
INSERT INTO Saber VALUES ( 84, 69, '2-4-2013');
INSERT INTO Saber VALUES ( 189, 12, '24-9-1977');
INSERT INTO Saber VALUES ( 204, 104, '7-11-1979');
INSERT INTO Saber VALUES ( 231, 52, '6-10-2010');
INSERT INTO Saber VALUES ( 23, 41, '14-6-2000');
INSERT INTO Saber VALUES ( 272, 65, '17-2-2019');
INSERT INTO Saber VALUES ( 178, 39, '24-12-1962');
INSERT INTO Saber VALUES ( 169, 56, '7-2-1967');
INSERT INTO Saber VALUES ( 209, 34, '10-1-1982');
INSERT INTO Saber VALUES ( 172, 59, '3-10-2020');
INSERT INTO Saber VALUES ( 71, 45, '27-1-2005');
INSERT INTO Saber VALUES ( 255, 76, '16-9-1976');
INSERT INTO Saber VALUES ( 110, 78, '4-6-2005');
INSERT INTO Saber VALUES ( 80, 122, '8-1-2015');
INSERT INTO Saber VALUES ( 173, 64, '4-2-1983');
INSERT INTO Saber VALUES ( 236, 9, '23-12-2019');
INSERT INTO Saber VALUES ( 203, 71, '23-3-1988');
INSERT INTO Saber VALUES ( 55, 75, '19-10-1986');
INSERT INTO Saber VALUES ( 70, 64, '11-5-1994');
INSERT INTO Saber VALUES ( 67, 127, '14-11-1972');
INSERT INTO Saber VALUES ( 115, 72, '28-3-1995');
INSERT INTO Saber VALUES ( 9, 93, '13-3-1993');
INSERT INTO Saber VALUES ( 245, 37, '8-3-1980');
INSERT INTO Saber VALUES ( 135, 57, '5-9-2012');
INSERT INTO Saber VALUES ( 72, 69, '25-8-1990');
INSERT INTO Saber VALUES ( 172, 113, '14-2-1974');
INSERT INTO Saber VALUES ( 221, 80, '8-3-1997');
INSERT INTO Saber VALUES ( 89, 51, '20-11-1967');
INSERT INTO Saber VALUES ( 167, 3, '8-1-1964');
INSERT INTO Saber VALUES ( 146, 36, '27-11-1974');
INSERT INTO Saber VALUES ( 162, 67, '2-8-2005');
INSERT INTO Saber VALUES ( 235, 30, '19-2-2014');
INSERT INTO Saber VALUES ( 124, 57, '19-7-1967');
INSERT INTO Saber VALUES ( 246, 98, '13-3-1987');
INSERT INTO Saber VALUES ( 282, 71, '3-8-1984');
INSERT INTO Saber VALUES ( 47, 93, '25-6-2015');
INSERT INTO Saber VALUES ( 115, 110, '27-7-2003');
INSERT INTO Saber VALUES ( 262, 87, '15-9-1977');
INSERT INTO Saber VALUES ( 239, 140, '24-6-1964');
INSERT INTO Saber VALUES ( 44, 28, '15-8-1999');

--select * from Personagem;
--select * from Casa;
--select * from Feitico;
--select * from Varinha;

SELECT personagem.nomePersonagem, personagem.dataNasc, saber.dataAprendeu, feitico.nomeFeitico
FROM Feitico NATURAL JOIN Saber NATURAL JOIN Personagem 
WHERE personagem.personagempk = saber.personagemfk AND saber.feiticoFK = Feitico.feiticoPK;

/* -------- PRIMEIRA FUNÇÃO --------
	Mostra os dados mais relevantes de um determinado personagem,
	dada sua PK passada por parâmetro.
*/
DROP FUNCTION IF EXISTS mostraDadosPersonagem;
CREATE OR REPLACE FUNCTION mostraDadosPersonagem(PK integer)
RETURNS TABLE (nome varchar(50), data_de_nascimento date, pontos_acumulados int,
			   casa varchar(15), pontos_da_casa int) AS $$
BEGIN
	RETURN QUERY SELECT Personagem.nomePersonagem,
		   Personagem.dataNasc,
		   Personagem.pontosAcumulados,
		   Casa.nomeCasa,
		   Casa.pontos
	FROM Personagem NATURAL JOIN Casa
	WHERE Personagem.personagemPK = PK AND
	   Personagem.casaFK = Casa.casaPK;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM mostraDadosPersonagem(50); -- Mostra dados do personagem cuja PK é 50

/* -------- SEGUNDA FUNÇÃO --------
	Dada a PK de um personagem e o número de pontos por parâmetro,
	computa os pontos ao personagem e sua respectiva casa, caso o
	personagem pertença a uma das casas.
*/
DROP FUNCTION IF EXISTS marcaPontos;
CREATE OR REPLACE FUNCTION marcaPontos(PK integer, pontos_inc integer)
RETURNS VOID AS $$
DECLARE
	pontos_personagem integer; -- número de pontos que o personagem tem atualmente
	personagem_casa integer;   -- PK da casa do personagem
	pontos_casa integer;       -- número de pontos que a casa tem atualmente
BEGIN
	SELECT pontosAcumulados INTO pontos_personagem
	FROM Personagem WHERE personagemPK = PK;
	SELECT casaFK INTO personagem_casa
	FROM Personagem WHERE personagemPK = PK;
	SELECT pontos INTO pontos_casa
	FROM Casa WHERE personagem_casa = casaPK;
-- Computa os valores novos para os pontos se a casa for válida
	IF personagem_casa IS NOT NULL THEN
		pontos_personagem = pontos_personagem + pontos_inc;
		pontos_casa = pontos_casa + pontos_inc;
	END IF;
-- Dá update nas tabelas
	UPDATE Personagem
	SET pontosAcumulados = pontos_personagem
	WHERE personagemPK = PK;
	UPDATE Casa
	SET pontos = pontos_casa
	WHERE casaPK = personagem_casa;
END;
$$ LANGUAGE plpgsql;

/* -------- TERCEIRA FUNÇÃO --------
	Zera os pontos de uma casa, e por consequência, de todos
	os Personagens dessa casa.
*/
DROP FUNCTION IF EXISTS zerarPontos;
CREATE OR REPLACE FUNCTION zerarPontos(PK_zerar integer)
RETURNS VOID AS $$
BEGIN
	UPDATE Personagem
	SET pontosAcumulados = 0
	WHERE casaFK = PK_zerar;
	UPDATE Casa
	SET pontos = 0
	WHERE casaPK = PK_zerar;
END;
$$ LANGUAGE plpgsql;

SELECT zerarPontos(2);
SELECT marcapontos(2, 5); 
SELECT Personagem.nomePersonagem, Casa.nomeCasa, Personagem.pontosAcumulados, Casa.pontos as pontos_casa
FROM Personagem INNER JOIN Casa
ON Personagem.casaFK = Casa.casaPK
ORDER BY Personagem.pontosAcumulados DESC;

SET enable_seqscan = OFF;

DROP INDEX IF EXISTS idxNomePers;
CREATE INDEX IF NOT EXISTS idxNomePers ON Personagem (nomePersonagem);
--EXPLAIN ANALYZE SELECT * FROM Personagem WHERE nomePersonagem = 'Harry James Potter';

DROP INDEX IF EXISTS idxPontosCasaPers;
CREATE INDEX IF NOT EXISTS idxPontosCasaPers ON Personagem (pontosAcumulados, casaFK);
--EXPLAIN ANALYZE SELECT * FROM Personagem WHERE casaFK = 1;

DROP INDEX IF EXISTS idxVarinhaDono;
CREATE INDEX IF NOT EXISTS idxVarinhaDono ON Varinha (varinhaPK, donoFK);
--EXPLAIN ANALYZE SELECT * FROM Varinha WHERE varinhaPK = 1;


DROP INDEX IF EXISTS idxVarinhaCarac;
CREATE INDEX IF NOT EXISTS idxVarinhaCarac ON Varinha (material, nucleo, medida, flex);
--EXPLAIN ANALYZE SELECT * FROM Varinha WHERE material = 'Cherry';

DROP INDEX IF EXISTS idxFeiticoAlcance;
CREATE INDEX IF NOT EXISTS idxFeiticoAlcance ON Feitico (feiticoPK, alcance);
--EXPLAIN ANALYZE SELECT * FROM Feitico WHERE alcance > 8;

DROP INDEX IF EXISTS idxSaber;
CREATE INDEX IF NOT EXISTS idxSaber ON Saber (feiticoFK, personagemFK, dataAprendeu);
--EXPLAIN ANALYZE SELECT * FROM Saber WHERE extract(year from dataAprendeu) > 1985;

-- RECALCULAR OS PONTOS TOTAIS DA CASA SEMPRE QUE UMA TUPLA NA TABELA PERSONAGEM FOR INSERIDO OU ATUALIZADO


CREATE OR REPLACE FUNCTION AtualizaPontos()
RETURNS trigger AS $teste$
DECLARE 
	var INT;
BEGIN
	SELECT SUM( PontosAcumulados ) INTO var FROM Personagem WHERE Personagem.CasaFK = NEW.CasaFK;
    UPDATE Casa SET Pontos = var WHERE CasaPK = NEW.CasaFK; 
    RETURN NULL;
END;
$teste$ LANGUAGE plpgsql;

--DROP TRIGGER TriggerPontosDasCasas ON Personagem CASCADE;
CREATE TRIGGER TriggerPontosDasCasas
AFTER INSERT OR UPDATE ON Personagem
FOR EACH ROW
EXECUTE PROCEDURE AtualizaPontos();

-- Auditoria
DROP TABLE IF EXISTS Audit_saber;
CREATE TABLE Audit_saber (operacao CHAR, data TIMESTAMP, usuario VARCHAR, feitico INTEGER, personagem INTEGER);

CREATE OR REPLACE FUNCTION process_saber_audit()
RETURNS TRIGGER AS $saber_audit$
BEGIN 
    IF (TG_OP = 'DELETE') THEN 
        INSERT INTO audit_saber SELECT 'D', NOW(), USER, OLD.feiticoFK, OLD.personagemFK;
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN 
        INSERT INTO audit_saber SELECT 'U', NOW(), USER, NEW.feiticoFK, NEW.personagemFK;
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN 
        INSERT INTO audit_saber SELECT 'I', NOW(), USER, NEW.feiticoFK, NEW.personagemFK;
        RETURN NEW;
    END IF;

    RETURN NULL;
END;
$saber_audit$ LANGUAGE plpgsql;

CREATE TRIGGER saber_audit
AFTER INSERT OR UPDATE OR DELETE ON saber
FOR EACH ROW EXECUTE PROCEDURE process_saber_audit();


-- Visao materializada
-- talvez precise de um update materialized view dentro das triggers

CREATE MATERIALIZED VIEW houses (nome, casa, pontos) 
AS SELECT
	Personagem.NomePersonagem,
    Casa.NomeCasa,
    Personagem.PontosAcumulados
FROM Personagem JOIN casa
ON Personagem.casaFK = Casa.CasaPK;

CREATE MATERIALIZED VIEW wands (nome, material, nucleo, medidas) 
AS SELECT
	Personagem.NomePersonagem,
    Varinha.Material,
    Varinha.Nucleo,
    Varinha.Medida
FROM Personagem JOIN Varinha
ON Personagem.personagemPK = Varinha.DonoFK;

CREATE MATERIALIZED VIEW spells (feitico, nome, alcance) 
AS SELECT
    Feitico.nomeFeitico,
	Personagem.nomePersonagem,
    Feitico.Alcance
FROM Feitico JOIN Saber ON Feitico.FeiticoPK = Saber.FeiticoFK
JOIN Personagem ON Personagem.PersonagemPK = Saber.PersonagemFK;

--select * from wands;
--select * from houses;
--select * from spells;